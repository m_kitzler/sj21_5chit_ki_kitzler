#!/usr/bin/env python
# coding: utf-8

# # Config erstellen
# Seite: [Training Pipelines & Models](https://spacy.io/usage/training)  
# Spache Englisch, Komponenten: ner, Hardware: CPU (GPU: macht Probleme), Optimieren für: Genauigkeit (Effizienz: F16 wird nicht erkannt)

# In[1]:


import subprocess

print(subprocess.check_output('python -m spacy init fill-config ./custom_NER/base_config.cfg ./custom_NER/config.cfg'.split(' ')).decode('utf-8'))


# # Konvertierung zu .spacy

# In[3]:


import pandas as pd
from tqdm import tqdm
import spacy


# In[2]:


from spacy.tokens import DocBin

nlp = spacy.blank("en") # load a new spacy model
db = DocBin() # create a DocBin object

data = [('The F15 aircraft uses a lot of fuel', {'entities': [(4, 7, 'aircraft')]}),
 ('did you see the F16 landing?', {'entities': [(16, 19, 'aircraft')]}),
 ('how many missiles can a F35 carry', {'entities': [(24, 27, 'aircraft')]}),
 ('is the F15 outdated', {'entities': [(7, 10, 'aircraft')]}),
 ('does the US still train pilots to dog fight?',
  {'entities': [(0, 0, 'aircraft')]}),
 ('how long does it take to train a F16 pilot',
  {'entities': [(33, 36, 'aircraft')]}),
 ('how much does a F35 cost', {'entities': [(16, 19, 'aircraft')]}),
 ('would it be possible to steal a F15', {'entities': [(32, 35, 'aircraft')]}),
 ('who manufactures the F16', {'entities': [(21, 24, 'aircraft')]}),
 ('how many countries have bought the F35',
  {'entities': [(35, 38, 'aircraft')]}),
 ('is the F35 a waste of money', {'entities': [(7, 10, 'aircraft')]})];

for text, annot in tqdm(data): # data in previous format
    doc = nlp.make_doc(text) # create doc object from text
    ents = []
    for start, end, label in annot["entities"]: # add character indexes
        span = doc.char_span(start, end, label=label, alignment_mode="contract")
        if span is None:
            print("Skipping entity")
        else:
            ents.append(span)
    doc.ents = ents # label the text with the ents
    db.add(doc)

db.to_disk("./custom_NER/data.spacy") # save the docbin object


# # Trainieren

# In[5]:


import os
os.environ["KMP_DUPLICATE_LIB_OK"]="TRUE"

p = subprocess.run('python -m spacy train ./custom_NER/config.cfg --output ./custom_NER/output --paths.train ./custom_NER/data.spacy --paths.dev ./custom_NER/data.spacy'.split(' '), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
print(p.stdout.decode('utf-8'))
print(p.stderr.decode('utf-8'))


# # Test

# In[4]:


nlp1 = spacy.load(R".\custom_NER\output\model-best")
doc = nlp1("Did you see the F16 fly by just now?")

spacy.displacy.render(doc, style="ent", jupyter=True)

