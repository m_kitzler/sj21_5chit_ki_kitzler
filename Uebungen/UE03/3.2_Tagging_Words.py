#!/usr/bin/env python
# coding: utf-8

# # 3.2.1

# In[2]:


with open('dataScientistJobDesc.txt', 'r', encoding='utf8') as file:
    text = file.read()


# ### NLTK

# In[3]:


import nltk
nltk.download('punkt')
from nltk.tokenize import word_tokenize
nltk.download('averaged_perceptron_tagger')

words = [token[0] for token in nltk.pos_tag(word_tokenize(text)) if token[1][0:2] == 'NN']

print(len(words))


# ### Spacy

# In[4]:


import spacy
nlp = spacy.load('en_core_web_sm')

spacy_tokens = nlp(text)
words = [token for token in spacy_tokens if token.pos_ == 'NOUN' or token.pos_ == 'PROPN']

print(f"Count: {len(words)}")

for i in range(10):
    print(words[i].text, words[i].pos_)


# # 3.2.2

# In[5]:


from collections import Counter
dict(Counter(token.pos_ for token in spacy_tokens).most_common())


# # 3.2.3

# In[6]:


Counter((token.text for token in spacy_tokens)).most_common(1)

