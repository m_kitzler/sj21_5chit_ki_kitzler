#!/usr/bin/env python
# coding: utf-8

# # 3.3.1

# In[28]:


import spacy
from spacy.matcher import Matcher
nlp = spacy.load('en_core_web_sm')

doc = nlp('Hello, world! Hello world!')
matcher = Matcher(nlp.vocab)

pattern = [{'POS': 'INTJ'}, {'POS': 'PUNCT', 'OP': '?'}, {'POS': 'NOUN'}]

matcher.add('HelloWorld', [pattern])
matches = matcher(doc)

print("Total matches found:", len(matches))
for match_id, start, end in matches:
    print(match_id, nlp.vocab.strings[match_id], start, end, doc[start:end].text)


# # 3.3.2

# In[48]:


doc = nlp('''Experience with Data Visualization Tools
Experience with Data Science Tools
Programming Experience''')
matcher = Matcher(nlp.vocab)

patternIn = [{'LOWER': 'experience'}, {'POS': 'ADP'}, {'POS': 'PROPN', 'OP': '+'}]
patternTypeof = [{'POS': 'NOUN', 'OP': '+'}, {'LOWER': 'experience'}]

matcher.add('pattern', [patternIn, patternTypeof])
matches = matcher(doc)

spans = [doc[start:end] for _, start, end in matches]
for span in spacy.util.filter_spans(spans):
    print(span)

