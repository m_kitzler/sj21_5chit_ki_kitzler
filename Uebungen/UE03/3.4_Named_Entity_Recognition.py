#!/usr/bin/env python
# coding: utf-8

# # 3.4.1

# In[2]:


import spacy
nlp = spacy.load('en_core_web_sm')

doc = nlp('Apple is looking at buying U.K. startup for $1 billion')

for ent in doc.ents:
	print(ent.text, '\t', ent.start_char, '\t', ent.end_char, '\t', ent.label_, '\t', spacy.explain(ent.label_))


# In[3]:


from spacy import displacy
displacy.render(doc, style="ent")


# # 3.4.2

# In[4]:


print('GPE:\t', spacy.explain('GPE'))
print('NORP:\t', spacy.explain('NORP'))
print('ORDINAL:', spacy.explain('ORDINAL'))


# # 3.4.3

# In[5]:


text = 'Dear Joe! I have organized a meeting with Elon Musk from Siemens for tomorrow. Meeting place is Vienna.'#
doc = nlp(text)

persons = [ent for ent in doc.ents if ent.label_ == 'PERSON']

for person in persons:
	print(person)


# In[6]:


for token in doc:
    print(f'{token.text:10} {token.ent_iob_} {token.ent_type_}')


# # 3.4.4

# In[8]:


nlp = spacy.load('de_core_news_sm')

text = '''Der Ministerrat der Republik Österreich beschloss am 25.März 2014, eine „Unabhängige
Untersuchungskommission zur transparenten Aufklärung der Vorkommnisse rund um die
Hypo Group Alpe-Adria“ einzusetzen. Die Untersuchungskommission (Manuel Ammann, Carl
Baudenbacher, Ernst Wilhelm Contzen, Irmgard Griss, Claus-Peter Weber) hat, beginnend
mit 1.Mai 2014, durch Auswertung von beigeschafften Unterlagen und allgemein zugänglchen
Quellen sowie durch Befragung von Auskunftspersonen den maßgeblichen Sachverhalt
festgestellt und nach fachlichen Kriterien bewertet.'''
doc = nlp(text)

displacy.render(doc, style="ent")

## ohne retokenize
# for ent in reversed(doc.ents): #reversed to not modify the offsets of other entities when substituting
# 	if (ent.label_ == 'PER'):
# 		start = ent.start_char
# 		end = start + len(ent.text)
# 		text = text[:start] + '[REDACTED]' + text[end:]
# print(text)

# mit retokenize
with doc.retokenize() as retokenizer:
	for ent in doc.ents:
		if (ent.label_ == 'PER'):
			retokenizer.merge(doc[ent.start:ent.end])
print("".join(['[REDACTED]' if t.ent_type_ == 'PER' else t.text_with_ws for t in doc]))

