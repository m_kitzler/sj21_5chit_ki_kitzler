#!/usr/bin/env python
# coding: utf-8

# # 3.1.1

# ## Schritte des Text Processing:
# 1. Cleaning  
# Entfernung unwichtiger Information, wie Html-Tags  
# 
# 2. Normalization  
# Ganzen Text klein schreiben  
# 
# 3. Tokenization   
# Aufteilen in Liste von Wörtern  
# 
# 4. Stop Words removal  
# Stoppwörter (a, an, the, ...) entfernen  
# 
# 5. Parts of Speech Tagging  
# Identifikation der Wortarten  
# 
# 6. Named Entity Recognition  
# Namen erkennen  
# 
# 7. Stemming and Lemmatization  
# "Stemming": runs -> run  
# "Lemmatization": Wort wird wörterbuchartig vereinfacht, Bsp. "is" und "are" kommen Beide von "be"

# # 3.1.2

# In[22]:


tweets=[ 
    'This is introduction to NLP', 

    'It is likely to be useful, to people ', 
    'Machine learning is the new electrcity', 
    'There would be less hype around AI and more action going for-ward', 
    'python is the best tool!','R is good langauage', 
    'I like this book','I want more books like this' 
]


# In[23]:


import pandas as pd

df_tweets = pd.DataFrame(tweets, columns=['tweet'])
df_tweets = df_tweets.applymap(lambda x: x.lower())
df_tweets


# # 3.1.3

# In[24]:


import re

df_tweets['tweet'] = df_tweets.apply(lambda x: re.sub(r"[\.!?,]", '', x['tweet']), axis=1)
df_tweets


# # 3.1.4

# In[25]:


import nltk
nltk.download('punkt')
from nltk.tokenize import word_tokenize
text = df_tweets.loc[4][0]
print(word_tokenize(text))
print(str(text).split(' '))


# nltk arbeitet "intelligenter"

# # 3.1.5

# In[26]:


df_tweets['tokenized'] = df_tweets.apply(lambda x: word_tokenize(x['tweet']), axis=1)
df_tweets


# # 3.1.6

# In[27]:


#text = re.sub(r"[.,!?‘’;:()\n—–-]", '', text.lower())
#text = re.sub(r"/", ' ', text)


# In[28]:


with open('.\\dataScientistJobDesc.txt', 'r', encoding='utf8') as file:
    text = file.read()

tokens = word_tokenize(text)
print(f"Tokens: {len(tokens)}")
unique_tokens = set(tokens)
if len(tokens) > len(unique_tokens):
    tokens = unique_tokens
print(f"Unique Tokens: {len(tokens)}")


# # 3.1.7

# In[29]:


import spacy
nlp = spacy.load('en_core_web_sm')

spacy_tokens = nlp(text)
print(f"Tokens: {len(spacy_tokens)}")
unique_tokens = set(map(str, spacy_tokens))
if len(spacy_tokens) > len(unique_tokens):
    spacy_tokens = unique_tokens
print(f"Unique Tokens: {len(spacy_tokens)}")


# In[30]:


df = pd.DataFrame(list(spacy_tokens.difference(tokens)), columns=['spacy_tokens'])
df['tokens'] = pd.Series(list(tokens.difference(spacy_tokens)))
df


# Der Hauptunterschied ist, dass Spacy erkennt, wenn zwei oder mehrere Wörter mit Satzzeichen verbunden werden.

# # 3.1.8

# How wurde nicht kleingeschreiben.

# # 3.1.9

# In[31]:


nltk.download('stopwords')
from nltk.corpus import stopwords

stop = stopwords.words('english')
df_tweets['without stopwords'] = df_tweets.apply(lambda x: " ".join([word for word in x['tweet'].split() if word not in stop]), axis=1)
df_tweets


# # 3.1.10

# In[32]:


from nltk.stem import PorterStemmer
pt = PorterStemmer()

text = ['I like fishing','I eat fish','There are many fishes in pound']
s_text = pd.Series(text)
s_text.apply(lambda x: " ".join([pt.stem(word) for word in x.split()]))


# # 3.1.11

# In[33]:


nltk.download('wordnet')
from nltk.stem import WordNetLemmatizer
wnl = WordNetLemmatizer()

text = ['I like fishing','I eat fish','There are many fishes in pound']
s_text = pd.Series(text)
s_text.apply(lambda x: " ".join([wnl.lemmatize(word) for word in x.split()]))

