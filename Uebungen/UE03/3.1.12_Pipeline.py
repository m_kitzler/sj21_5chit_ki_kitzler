#!/usr/bin/env python
# coding: utf-8

# # 3.12

# In[8]:


import regex as re
import nltk
nltk.download('stopwords')
from nltk.corpus import stopwords
stop = stopwords.words('english')

def to_lower(text_to_process):
    return str(text_to_process).lower()

def remove_url(text_to_process, replacement_text = '__URL__'):
    return re.sub(r"https?:\/\/\w+(\.\w+)+(\/[\w-]+)*\/?", replacement_text, text_to_process)

def remove_email(text_to_process, replacement_text = '__EMAIL__'):
    return re.sub(r"[\w.-]+@\w+(\.\w+)+", replacement_text, text_to_process)

def remove_punctuation(text_to_process):
    return re.sub(r"\p{P}", '', text_to_process)

def remove_stopword(text_to_process):
    return " ".join([word for word in text_to_process.split() if word not in stop])

def preprocess_text(text_to_process, processing_function_list): 
    text = text_to_process
    for func in processing_function_list:
        text = func(text)
    return text


# In[9]:


preprocess_functions = [to_lower, remove_url, remove_email, remove_punctuation]

text = '''Ziel  test@test.com ist  die  Entwicklung  einer  Text  Preprocessing  Pipeline.  Klingt  kompliziert,  ist  es  aber  
nicht. Im Grunde geht es um die Entwicklung eines Skripts, das n-viele Funktionen definiert, 

die  man  nach  Bedarf  aufruft.'''
preprocess_text(text, preprocess_functions)

