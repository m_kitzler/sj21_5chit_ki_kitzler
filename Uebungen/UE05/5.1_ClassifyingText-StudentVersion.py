#!/usr/bin/env python
# coding: utf-8

# In[4]:


import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer,TfidfVectorizer
from sklearn import model_selection, preprocessing, linear_model, naive_bayes, metrics, svm


# ## Datenaufbereitung
# 1. Laden Sie das *spam.csv*-File in das DataFrame *data*
# 2. Ändern Sie die Spaltenbezeichner: v1 -> **target** und v2 -> **message**
# 3. Transfomieren Sie den Inhalt der Spalte *message* in Kleinbuchstaben
# 4. Entfernen Sie Stoppwörter
# 5. Führen Sie Stemming und Lemmatisierung durch

# In[5]:


data = pd.read_csv("spam.csv", header=0, names=["target", "message", "1", "2", "3"])
data["message"] = data.loc[:, "message":"3"].apply(lambda x: ",".join([str(word) for word in x if word is not np.NaN]), axis=1)
data = data[["target","message"]]

# Ergebnis:
data.head(10)


# In[9]:


import nltk
from nltk.stem import WordNetLemmatizer#, PorterStemmer
from nltk.corpus import stopwords

nltk.download('stopwords')
lem = WordNetLemmatizer()
#stem = PorterStemmer()

def preprocess(text):
	temp = nltk.word_tokenize(text)
	temp = [lem.lemmatize(word.lower()) for word in temp]
	return " ".join([word for word in temp if word not in stopwords.words("english")])


# In[10]:


data["message"] = data["message"].apply(preprocess)

data.head(10)


# Das Dataset *data* mithilfe von `train_test_split()` in Trainings- und Validierungsdaten aufteilen:

# In[19]:


train, valid = train_test_split(data, test_size=.3)
print("Check shape: ", train.shape, valid.shape)


# Die Antworten (*ham* o. *spam*) in 1 oder 0 umwandeln:

# In[24]:


encoder = preprocessing.LabelEncoder()
train["target"] = encoder.fit_transform(train["target"])
valid["target"] = encoder.fit_transform(valid["target"])

print("Encoder hat mit 0 und 1 zwei Klassen erkannt: ", encoder.classes_)
print("Die kodierten Antworten der Trainingsdaten: " , train["target"])


# Vektorisierung der Daten mit TF-IDF. Mit der Größe von `max_features` kann experimentiert werden. Diese legt den Umfang des Vokabulars fest.

# In[34]:


vectorizer = TfidfVectorizer(analyzer='word', max_features=5000)
vectorizer.fit(data['message'])

print("Vektor mit 5000 Einträgen: ",  vectorizer.get_feature_names_out().shape)
print("Vokabular: ", vectorizer.get_feature_names_out())


# In[35]:


train_x_tfidf = vectorizer.transform(train["message"])
valid_x_tfidf = vectorizer.transform(valid["message"])
print("Check expected shape for train data: ", train_x_tfidf.shape)
print("Check expected shape for validation data: ", valid_x_tfidf.shape)


# Classifier trainieren und validieren:

# In[36]:


clf = naive_bayes.MultinomialNB(alpha=0.2)
# fit the training dataset on the classifier
clf.fit(train_x_tfidf, train["target"])

# predict the labels on validation dataset
predictions = clf.predict(valid_x_tfidf)

# retrieve accuracy score
metrics.accuracy_score(predictions, valid["target"])

