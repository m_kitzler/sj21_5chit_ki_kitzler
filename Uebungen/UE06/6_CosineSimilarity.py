#!/usr/bin/env python
# coding: utf-8

# In[1]:


from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

documents = (
"I like NLP",
"I am exploring NLP",
"I am a beginner in NLP",
"I want to learn NLP",
"I like advanced NLP"
)


# In[2]:


tfidf_vectorizer = TfidfVectorizer()
tfidf_matrix = tfidf_vectorizer.fit_transform(documents)
tfidf_matrix.toarray()


# In[3]:


cosine_similarity(tfidf_matrix[0:1], tfidf_matrix)


# In[4]:


import spacy

nlp = spacy.load("en_core_web_lg")
cats_token, dogs_token = nlp("cats dogs")
cats_token.similarity(dogs_token)


# In[5]:


cats_token.vector


# In[6]:


sentence = 'This is a sentence.'
nlp(sentence).vector_norm


# In[7]:


doc1 = nlp("I like NLP")
doc2 = nlp("I like advanced NLP")
doc1.similarity(doc2)


# # 6.1

# In[8]:


import pandas as pd

faq = pd.read_csv("covid_faq.csv")

def calculate_similarity(text1, text2):
	return nlp(text1).similarity(nlp(text2))

def search(text):
	return faq.iloc[faq.apply(lambda x: calculate_similarity(x["questions"], text), axis=1).idxmax()]["answers"]


# In[9]:


search("test")


# # 6.2: FuzzyWuzzy (oder jetzt offiziell "thefuzz")

# ### ratio

# In[10]:


from thefuzz import fuzz
print(fuzz.ratio('Deluxe Room, 1 King Bed', 'Deluxe King Room'))
print(fuzz.ratio('Traditional Double Room, 2 Double Beds', 'Double Room with Two Double Beds'))
print(fuzz.ratio('Room, 2 Double Beds (19th to 25th Floors)', 'Two Double Beds - Location Room (19th to 25th Floors)'))


# In[11]:


faq["accuracy"] = faq.apply(lambda x: fuzz.ratio(x["questions"], "test"), axis=1)
faq.head()


# ### partial_ratio
# compares partial string similarity

# In[12]:


print(fuzz.partial_ratio('Deluxe Room, 1 King Bed', 'Deluxe King Room'))
print(fuzz.partial_ratio('Traditional Double Room, 2 Double Beds', 'Double Room with Two Double Beds'))
print(fuzz.partial_ratio('Room, 2 Double Beds (19th to 25th Floors)', 'Two Double Beds - Location Room (19th to 25th Floors)'))


# In[13]:


faq["accuracy"] = faq.apply(lambda x: fuzz.partial_ratio(x["questions"], "test"), axis=1)
faq.head()


# ### token_sort_ratio
# ignores word order

# In[14]:


print(fuzz.token_sort_ratio('Deluxe Room, 1 King Bed', 'Deluxe King Room'))
print(fuzz.token_sort_ratio('Traditional Double Room, 2 Double Beds', 'Double Room with Two Double Beds'))
print(fuzz.token_sort_ratio('Room, 2 Double Beds (19th to 25th Floors)', 'Two Double Beds - Location Room (19th to 25th Floors)'))


# In[15]:


faq["accuracy"] = faq.apply(lambda x: fuzz.token_sort_ratio(x["questions"], x["answers"]), axis=1)
faq.head()


# ### token_set_ratio
# ignores duplicated words. It is similar with token sort ratio, but a little bit more flexible.

# In[16]:


print(fuzz.token_set_ratio('Deluxe Room, 1 King Bed', 'Deluxe King Room'))
print(fuzz.token_set_ratio('Traditional Double Room, 2 Double Beds', 'Double Room with Two Double Beds'))
print(fuzz.token_set_ratio('Room, 2 Double Beds (19th to 25th Floors)', 'Two Double Beds - Location Room (19th to 25th Floors)'))


# In[17]:


faq["accuracy"] = faq.apply(lambda x: fuzz.token_set_ratio(x["questions"], x["answers"]), axis=1)
faq.head()


# # 6.3 Phonetic matching mit Fuzzy

# In[ ]:




