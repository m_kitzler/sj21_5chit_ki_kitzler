# %% [markdown]
# # 9.2

# %%
import os

# %%
def loadText(path):
	text = ""
	for root, folders, files in os.walk(path):
		for file in files:
			with open(path + "\\" +file, encoding="utf-8") as f:
				text = text + f.read()
	return text

paths = [".\\Texte\\bedrohung_des_freien_worts", ".\\Texte\\Die neue Unverbindlichkeit", ".\\Texte\\fliegen_ist_elitaer", ".\\Texte\\ReportEnergy"]
text = loadText(paths[1])

# %% [markdown]
# # 9.3

# %%
import spacy
nlp = spacy.load("de_core_news_sm")

# %%
tokens_all = nlp(text)
print(len(tokens_all))

# %%
len(set(tokens_all))

# %%
len([token for token in tokens_all if not str(token).isnumeric()])

# %% [markdown]
# ### a)

# %%
from sklearn.feature_extraction.text import CountVectorizer
cv = CountVectorizer()
cv.fit([str(token) for token in tokens_all])
features = cv.get_feature_names_out()
print("Length:", len(features))
features[:10]

# %% [markdown]
# ### b)

# %%
from sklearn.feature_extraction.text import CountVectorizer
cv = CountVectorizer(lowercase=False, token_pattern=".*")
cv.fit([str(token) for token in tokens_all])
features = cv.get_feature_names_out()
print("Length:", len(features))
features[:10]

# %% [markdown]
# ### c)

# %%
from sklearn.feature_extraction.text import CountVectorizer
cv = CountVectorizer(lowercase=False, token_pattern=".*", max_features=2000)
cv.fit([str(token) for token in tokens_all])
features = cv.get_feature_names_out()
len(features)

# %% [markdown]
# # 9.5

# %%
word_to_int = cv.vocabulary_
print(word_to_int.get("Meinungsrede"))
int_to_word = {v: k for k, v in word_to_int.items()}
print(int_to_word.get(389))

# %% [markdown]
# # 9.6

# %%
tokens_transformed = [word_to_int.get(str(token)) for token in tokens_all if word_to_int.get(str(token)) != None]
print(tokens_transformed[:10])
print("All: ", len(tokens_all))
print("Transformed:", len(tokens_transformed))


