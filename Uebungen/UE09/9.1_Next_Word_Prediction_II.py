# %%
import os
import spacy
from sklearn.feature_extraction.text import CountVectorizer
import numpy as np

nlp = spacy.load("de_core_news_sm")

# %%
def loadText(path):
	text = ""
	for root, folders, files in os.walk(path):
		for file in files:
			with open(path + "\\" +file, encoding="utf-8") as f:
				text = text + f.read()
	return text

paths = [".\\Texte\\bedrohung_des_freien_worts", ".\\Texte\\Die neue Unverbindlichkeit", ".\\Texte\\fliegen_ist_elitaer", ".\\Texte\\ReportEnergy"]
text = loadText(paths[1])

# %%
tokens_all = nlp(text)
cv = CountVectorizer(lowercase=False, token_pattern=".*", max_features=2000)
cv.fit([str(token) for token in tokens_all])
word_to_int = cv.vocabulary_
int_to_word = {v: k for k, v in word_to_int.items()}
tokens_transformed = [word_to_int.get(str(token)) for token in tokens_all if word_to_int.get(str(token)) != None]

# %%
def create_sqeuences(tokens_transformed, sequence_length):
	X = []; Y = []
	if (len(tokens_transformed) > sequence_length):
		for i in range(0, len(tokens_transformed) - sequence_length):
			X.append(tokens_transformed[i:sequence_length+i])
			Y.append(tokens_transformed[sequence_length+i])
	return X, Y

# %% [markdown]
# # 9.1

# %%
x, y = create_sqeuences(tokens_transformed, 40)
print(np.array(x[:2]).shape)
print(x[:2])
print(np.array(y[:2]).shape)
print(y[:2])


