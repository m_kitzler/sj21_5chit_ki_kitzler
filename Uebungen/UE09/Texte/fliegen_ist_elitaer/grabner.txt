Leserbrief: Corona und das Klima 

Im nachhaltigen Interview „Fliegen ist elitär“ erzählt der deutsche Tourismusforscher und Universitätsprofessor Stefan Gössling über den Klimawandel und das Fliegen.

Herr Gössling antwortet auf die Frage, wie lange es noch dauern wird, dass wir hoffentlich diesen Sommer wieder mit dem Flugzeug unterwegs sein dürfen. Laut seiner Aussagen haben es Tourismusländer derzeit besonders schwer und müssen sich leider noch gedulden. Leider sind diese Länder sehr auf unseren Tourismus angewiesen. Der gute Herr ist sehr kritisch gegenüber dem Reisen eingestellt und möchte es am liebsten direkt abschaffen. Für ihn zählt nur das wunderschöne Klima. Der Forscher möchte, dass wir unser Leben stark einschränken, sein oberstes Ziel ist die Rettung des Klimas.

Wenn laut seinen Aussagen nur 11 Prozent der ganzen Weltbevölkerung fliegen, dann kann dieses Thema nicht so wichtig sein, dass man es immer wieder öffentlich erwähnen muss. Das Ziel dieses Professors ist es, uns, als kleine Bürger auf dieser Erde, die Verantwortung für die großen Klimaverstöße zuzuschreiben. Vielmehr müssen große Konzerne, welche sich nicht für die Umwelt und das Klima interessieren, zur Rechenschaft gezogen werden. 

Ich appelliere an alle Leser und Leserinnen, sich nicht immer als Sündenbock für die aktuelle Klimakrise zu sehen und die Verantwortung den großen Konzernen zuzuschreiben.
