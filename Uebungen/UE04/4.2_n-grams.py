#!/usr/bin/env python
# coding: utf-8

# In[2]:


text = "I love NLP and I will learn NLP in two month "


# In[11]:


from sklearn.feature_extraction.text import CountVectorizer
vectorizer = CountVectorizer(ngram_range=(2,2))
vectorizer.fit([text])
vectorizer.vocabulary_


# # 4.2.1
# [0 0 0 1 0 0 0 0]: love nlp  
# [0 0 0 0 0 0 1 0]: two month  
# [0 0 0 0 0 1 0 0]: nlp in

# # 4.2.2

# In[59]:


import pandas as pd
tweets_df = pd.read_csv("realdonaldtrump.csv")
tweets = tweets_df["content"]
tweets


# In[75]:


from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize

stop = stopwords.words("english")
lemmatizer = WordNetLemmatizer()

def preprocess(text):
	words = word_tokenize(text)
	words = [lemmatizer.lemmatize(word.lower()) for word in words if not word in stop]
	return words

tweets_pre = tweets.apply(preprocess)


# In[76]:


from collections import Counter
from nltk import ngrams

n = 2

cnt = Counter(ngrams(tweets_pre[0], n))
for t in tweets[1:1000]:
	cnt += Counter(ngrams(t.split(" "), n))

cnt.most_common(10)


# In[80]:


from sklearn.feature_extraction.text import CountVectorizer
vectorizer = CountVectorizer(ngram_range=(2,2))

vectorizer.fit(tweets)
Counter(vectorizer.vocabulary_).most_common(10)


# In[82]:


vectorizer = CountVectorizer(ngram_range=(3,3))
vectorizer.fit(tweets)
Counter(vectorizer.vocabulary_).most_common(10)

