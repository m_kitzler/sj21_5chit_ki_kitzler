#!/usr/bin/env python
# coding: utf-8

# # 4.1

# In[1]:


import spacy
from sklearn.feature_extraction.text import CountVectorizer
from keras.utils import to_categorical
nlp = spacy.load('en_core_web_sm')


# In[2]:


def get_spacy_tokens(text):
    doc = nlp(text)
    return [token.text for token in doc]


# In[3]:


def one_hot_encoding(text):
	tokens = get_spacy_tokens(text)
	vectorizer = CountVectorizer(tokenizer=get_spacy_tokens, lowercase=False)
	vectorizer.fit(tokens)
	dict = vectorizer.vocabulary_
	result = []
	for t in tokens:
		result.append(list(to_categorical(dict[t], num_classes=len(dict))))
	return result


# In[5]:


text="Jim loves NLP. He will learn NLP in two monts. NLP is future."

# text_tokens = get_spacy_tokens(text)

# vectorizer = CountVectorizer(tokenizer=get_spacy_tokens, lowercase=False)

# # Erstellung des Vokabulars:
# vectorizer.fit(text_tokens)
# print("Vocabulary: ", vectorizer.vocabulary_)

matrix = one_hot_encoding(text)

matrix

