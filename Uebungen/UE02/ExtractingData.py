#!/usr/bin/env python
# coding: utf-8

# # 2.1

# In[18]:


import PyPDF2 as pypdf


# In[13]:


with open('schachnovelletext13.pdf', 'rb') as pdf:
    reader = pypdf.PdfFileReader(pdf)
    print(f"Author:\t\t{reader.documentInfo['/Author']}")
    print(f"Producer:\t{reader.documentInfo['/Producer']}")
    print(f"Title:\t\t{reader.documentInfo['/Title']}")


# # 2.2

# In[ ]:


with open('schachnovelletext13.pdf', 'rb') as pdf:
    reader = pypdf.PdfFileReader(pdf)
    print(reader.getPage(0).extractText())


# In[ ]:


with open('schachnovelletext13.pdf', 'rb') as pdf:
    reader = pypdf.PdfFileReader(pdf)
    for p in range(reader.numPages):
        print(f"Page {p + 1}:")
        print(reader.getPage(p).extractText())
        print()


# In[ ]:


pageList = []
with open('schachnovelletext13.pdf', 'rb') as pdf:
    reader = pypdf.PdfFileReader(pdf)
    for p in range(reader.numPages):
        pageList.append(reader.getPage(p).extractText())

for p in range(len(pageList)):
        print(f"Page {p + 1}:")
        print(pageList[p])
        print()


# # 2.3

# In[ ]:


import urllib.request as urllib2
from bs4 import BeautifulSoup

response = urllib2.urlopen('https://www.htlkrems.ac.at') 
html_doc = response.read() 

soup = BeautifulSoup(html_doc, 'html.parser') 

anchors = soup.findAll('a')

for a in anchors:
    print(a)


# # 2.4

# In[53]:


response = urllib2.urlopen('https://www.htlkrems.ac.at') 
html_doc = response.read() 

soup = BeautifulSoup(html_doc, 'html.parser') 

print(f"{len(soup.findAll('a'))} Anchors")
print(f"from which {len(soup.findAll('a', href=True))} are Hyperlinks")

