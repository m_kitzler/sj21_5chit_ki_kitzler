#!/usr/bin/env python
# coding: utf-8

# # 3.1

# In[3]:


import re

p = re.compile("[0-9]+\.[0-9]{2}[$€]") 
print(p.findall("Für 5 Semmeln habe ich 3.90€ bezahlt."))
print(p.findall("Für 5 Semmeln habe ich 3.90$ bezahlt."))


# # 3.2  
# " [0-9][0-9][0-9] " würde immer nach einer dreistelligen Zahl suchen.  
# Eine bessere lösung währe " [0-9]+ ", weil hier nach mindestens einer Zahl gesucht wird.

# # 3.3

# In[11]:


p = re.compile("\d+[\.,]\d{2}[$€]?") 
print(p.findall("Für 5 Semmeln habe ich 3.90€ bezahlt.")) 
print(p.findall("Für 15 Semmeln habe ich 19.50$ bezahlt.")) 
print(p.findall("Für 150 Semmeln habe ich 190,50 bezahlt."))


# # 3.4
# `finditer` gibt ein iterierbares Objekt zurück, `match` such nur am Anfang vom String, als würde man bei `search` ^ am Anfang des Patterns angeben.

# # 3.6

# In[29]:


import urllib.request as urllib2

response = urllib2.urlopen('https://www.htlkrems.ac.at') 
html_doc = response.read() 

p = re.compile("\w+@\w+[\.\w+]+")
with open('.\\KI5\\Uebungen\\UE02\\email.csv', 'w') as file:
    file.write('E-mails:\n')
    for m in p.finditer(str(html_doc)):
        val = m.group()
        file.write(val + '\n')
        print(val) 


# # 3.7

# In[12]:


import pandas as pd
from bs4 import BeautifulSoup

response = urllib2.urlopen('https://www.imdb.com/chart/top?ref_=nv_mv_250_6') 
html_doc = response.read()
soup = BeautifulSoup(html_doc, 'html.parser') 

movie_list = soup.find('tbody', class_='lister-list')
movies = movie_list.findAll('tr')

df_movies = pd.DataFrame(columns=['Titel', 'Jahr', 'Rating'])
for movie in movies:
    title_col = movie.find('td', class_='titleColumn')
    title = title_col.find('a').text
    year = title_col.find('span').text[1:-1]
    #year = re.search("\d+", year).group()

    rating_col = movie.find('td', class_='ratingColumn')
    rating = rating_col.find('strong').text

    df_movies = df_movies.append({'Titel':title, 'Jahr':int(year), 'Rating':float(rating)}, ignore_index=True)
df_movies

