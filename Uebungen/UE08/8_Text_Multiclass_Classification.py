#!/usr/bin/env python
# coding: utf-8

# In[52]:


import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score


# In[45]:


df = pd.read_csv("consumer_complaints.csv", encoding="utf-8")
df.head()


# # Step 1

# In[46]:


data = df[["product", "consumer_complaint_narrative"]]
data = data.dropna()
print(data.shape)
data


# # Step 2

# In[47]:


data["product"].value_counts()


# # Step 3

# In[48]:


X_train, X_test, y_train, y_test = train_test_split(data["consumer_complaint_narrative"], data["product"], train_size=0.7)
print(X_train.shape)
print(X_test.shape)
print(y_train.shape)
print(y_test.shape)


# # Step 4

# In[49]:


le = LabelEncoder()
le.fit(data["product"])
y_train = le.transform(y_train)
y_test = le.transform(y_test)


# In[50]:


vectorizer = TfidfVectorizer(analyzer="word", max_features=3000)
X_train = vectorizer.fit_transform(X_train)
X_test = vectorizer.fit_transform(X_test)
print(X_train.shape, X_test.shape)


# # Step 5

# In[53]:


rfc = RandomForestClassifier(max_depth=10, n_estimators=500, random_state=11)
rfc.fit(X_train, y_train)
accuracy_score(y_test, rfc.predict(X_test))

