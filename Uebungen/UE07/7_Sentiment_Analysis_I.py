#!/usr/bin/env python
# coding: utf-8

# # 7.1

# In[108]:


import pandas as pd
import nltk
nltk.download('vader_lexicon')

from nltk.sentiment.vader import SentimentIntensityAnalyzer
sid = SentimentIntensityAnalyzer()


# In[109]:


print(sid.polarity_scores("Good movie 10/10"))
print(sid.polarity_scores("Bad movie"))
print(sid.polarity_scores("There is no way that this movie gets a single bad review"))


# In[110]:


df = pd.read_csv("areviews.tsv", sep="\t")
df.head()


# In[111]:


df["label"].value_counts()


# In[112]:


df["score"] = df["review"].apply(lambda x: sid.polarity_scores(x))
df.head()


# In[113]:


df["compound"] = df["score"].apply(lambda x: x["compound"])
df.head()


# In[114]:


df["comp_score"] = df["compound"].apply(lambda x: "pos" if x >= 0 else "neg")
df.head()


# # 7.2

# In[115]:


from sklearn.metrics import classification_report

print(classification_report(df["label"], df["comp_score"]))


# In[116]:


from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay

print(confusion_matrix(df["label"], df["comp_score"]))
ConfusionMatrixDisplay.from_predictions(df["label"], df["comp_score"])


# # 7.3

# In[117]:


df = pd.read_csv("moviereviews.tsv", sep="\t")
df = df.dropna()
print(df["label"].value_counts())
df.head()


# In[118]:


df["pred"] = df["review"].apply(lambda x: "pos" if sid.polarity_scores(x)["compound"] >= 0 else "neg")
df.head()


# In[119]:


print(classification_report(df["label"], df["pred"]))
print(confusion_matrix(df["label"], df["pred"]))
ConfusionMatrixDisplay.from_predictions(df["label"], df["pred"])

