#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np


# In[2]:


#Phi, unsere Aktivierungsfunktion
def activation(value):
    #Implement the logic of the activation function (=> Step Function!) 
    #@Return: 0 or 1
    return 1 if value >= 0 else 0
    
#Sigma, liefert die gewichtete Summe zur Weiterverarbeitung mit 'activation'
def perceptron(x, w, b):
    #Implement the code needed for the perceptron. Use np.dot(...)
    return np.dot(x, w) + b
    #@Args: 
    #  x...value (vector => 1D Tensor), 
    #  w...weight (vector => 1D Tensor), 
    #  b...bias (just a value)
    #@Return: Value (Skalar eg. 1.0)


# In[3]:


# Test your code and check the results!
ex1 = np.array([0, 0])
ex2 = np.array([0, 1])
ex3 = np.array([1, 0])
ex4 = np.array([1, 1])

def OR_perceptron(x):
    # Define weights 
    w = np.array([1.0, 1.0])
    # Define Bias-value
    b = -1
    #Calculate Sigma
    sig = perceptron(x, w, b)
    #Call Acivation Function
    return activation(sig)

print(f"OR {ex1[0]},{ex1[1]}:", OR_perceptron(ex1))
print(f"OR {ex2[0]},{ex2[1]}:", OR_perceptron(ex2))
print(f"OR {ex3[0]},{ex3[1]}:", OR_perceptron(ex3))
print(f"OR {ex4[0]},{ex4[1]}:", OR_perceptron(ex4))

