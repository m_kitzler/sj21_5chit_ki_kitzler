#!/usr/bin/env python
# coding: utf-8

# In[9]:


class BankAccount:
    _balance = 0
    frozen = False
    
    def __init__(self, amount):
        self._balance = amount
    
    @property
    def balance(self):
        return self._balance
    
    def deposit(self, amount):
        if self.frozen:
            return "account is frozen"
        else:
            self._balance += amount
            return "deposited " + str(amount)
        
    def withdraw(self, amount):
        if self.frozen:
            return "account is frozen"
        else:
            if self._balance - amount >= -1000:
                self._balance -= amount
                return "withdrawn " + str(amount)
            else:
                return "not enought balance"
            
    def freeze(self):
        self.frozen = not self.frozen
        if self.frozen:
            return "account frozen"
        else:
            return "account unfrozen"


# In[12]:


import sys

ba = BankAccount(3500)
print(ba.deposit(10000))
print(ba.balance)

try:
    ba.balance = 9999999
except:
    print(sys.exc_info()[1])
print(ba.balance)

print(ba.withdraw(14500))
print(ba.balance)

print(ba.withdraw(11000))
print(ba.balance)

print(ba.freeze())
print(ba.withdraw(99999))
print(ba.freeze())
print(ba.deposit(100000))

