#!/usr/bin/env python
# coding: utf-8

# In[1]:


from decimal import *
import os


# In[2]:


with open('shopping_cart.txt', 'w') as f:
    f.write(('#Nr;Menge;Einzelpreis;Bezeichnung\n'
        '4711;2;100.20;Riesenschultüte\n'
        '0815;10;2.33;Kaugummi\n'
        '9365;2;5.11;Gemüse\n'
        '9999;55;100000.00;Gold\n'
        '8365;2;1.99;Karotte\n'
        '2856;1;0.50;Stein\n'
        '4175;5;6.99;Brot\n'
        '1263;2;0.99;Wasser 0.5L\n'
        '5532;3;10.99;Smart Water\n'
        '7391;5;5.23;Lebensmittel fraglicher Qualität\n'))
with open('broken_cart.txt', 'w') as f:
    f.write(('#Nr;Menge;Einzelpreis;Bezeichnung\n'
        ';2;100.20;Riesenschultüte\n'
        '0815;10;2.33;Kaugummi\n'
        '9365;2;5.11;Gemüse\n'
        '9999;55;100000.00;Gold\n'
        '8365;2;1.99;Karotte\n'
        '2856;1;0.50;Stein\n'
        '4175;5;6.99;Brot\n'
        '1263;2;0.99;Wasser 0.5L\n'
        '5532;3;10.99;Smart Water\n'
        '7391;5;5.23;Lebensmittel fraglicher Qualität\n'))


# In[3]:


class NoFileError(Exception):
    def __init__(self, message):
        self.message = message
        
class NoAccessError(Exception):
    def __init__(self, message):
        self.message = message

class cart:
    def __init__(self):
        self._items = []
    
    def addItem(self, item):
        self._items.append(item)
    
    @property
    def items(self):
        return self._items
        
    def toCSV(self):
        ret = '#Nr;Menge;Einzelpreis;Bezeichnung\n'
        for i in self._items:
            ret += i.toCSV() + '\n'
        return ret
    
    def totalToCSV(self):
        ret = '#Nr;Summe;Bezeichnung\n'
        for i in self._items:
            ret += i.totalToCSV() + '\n'
        return ret
    
    @staticmethod
    def fromFile(path):
        if os.path.isfile(path):
            c = cart()
            with open(path, 'r') as f:
                f.readline()
                read = f.readline()
                while not read == '':
                    c.addItem(item.fromCSV(read))
                    read = f.readline()
            return c
        else:
            raise NoFileError(f'File "{path}" does not exist')
            
    def toFile(self, path):
        dirPath = '.' if os.path.dirname(path) == '' else os.path.dirname(path)
        if os.access(dirPath, os.W_OK):
            with open(path, 'w') as f:
                f.writelines(c.totalToCSV())
        else:
            raise NoAccessError('No write access in this directory')


# In[4]:


class InvalidTypeError(Exception):
    def __init__(self, message):
        self.message = message
        
class ValueNullError(Exception):
    def __init__(self, message):
        self.message = message

class item:
    def __init__(self, nr, amount, price, desc):
        if isinstance(nr, int):
            self.nr = nr
        else:
            if nr.isdecimal():
                self.nr = int(nr)
            else:
                raise InvalidTypeError('Parameter "Number" must be of type int')
        
        if isinstance(amount, Decimal):
            self.amount = amount
        else:
            try:
                self.amount = Decimal(amount)
            except InvalidOperation:
                raise InvalidTypeError('Parameter "Amount" must be of type Decimal')
        
        if isinstance(price, Decimal):
            self.price = price
        else:
            try:
                self.price = Decimal(price)
            except InvalidOperation:
                raise InvalidTypeError('Parameter "Price" must be of type Decimal')
        
        if desc.strip():
            self.desc = desc
        else:
            raise ValueNullError('Parameter "Description" can\'t be null')
        
    @staticmethod
    def fromCSV(csv):
        data = csv.replace('\n', '').split(';')
        return item(data[0], data[1], data[2], data[3])
    
    def toCSV(self):
        return f'{self.nr};{self.amount};{self.price};{self.desc}'
    
    def totalToCSV(self):
        return f'{self.nr};{self.amount * self.price};{self.desc}'
    
    def __str__(self):
        return self.desc


# In[5]:


c = cart.fromFile('shopping_cart.txt')
print(c.totalToCSV())
with open('shopping_cart2.txt', 'w') as f:
    f.writelines(c.totalToCSV())


# In[6]:


c1 = cart.fromFile('broken_cart.txt')
print(c1.toCSV())


# In[7]:


cart.fromFile('test')


# In[8]:


item('', 12.50, 2, 'Item')

