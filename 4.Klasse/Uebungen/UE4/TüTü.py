#!/usr/bin/env python
# coding: utf-8

# In[1]:


class Passenger:
    def __init__(self, name):
        self._validTicket = False
        self.name = name
    
    @property
    def validTicket(self):
        return self._validTicket
    
    @validTicket.setter
    def validTicket(self, value):
        self._validTicket = value
        
    def getName(self):
        return self.name


# In[2]:


class Train:
    def __init__(self):
        self.passengers = []
        
    def stepIn(self, passenger):
        print(passenger.getName() + " stepped in")
        self.passengers.append(passenger)
        
    def stepOut(self, index):
        print(self.passengers[index].getName() + " stepped out")
        self.passengers.pop(index)
        
    def printPassengers(self):
        print("Current passengers:")
        for p in self.passengers:
            print("-" + p.getName())


# In[3]:


class RailJet(Train):
    def __init__(self):
        super().__init__()
    
    def stepIn(self, passenger):
        if passenger.validTicket:
            super().stepIn(passenger)
        else:
            print(passenger.getName() + " has no valid Ticket!")
            
    def printPassengers(self):
        super(RailJet, self).printPassengers()


# In[4]:


class InterCity(Train):
    def __init__(self, ICE_cat):
        self.ICE_Category = ICE_cat
        super().__init__()


# In[5]:


p1 = Passenger("Max")
p2 = Passenger("John")
p3 = Passenger("Anna")
p1.validTicket = True
p3.validTicket = True
rj = RailJet()
rj.stepIn(p1)
rj.stepIn(p2)
rj.stepIn(p3)
rj.printPassengers()
rj.stepOut(1)
rj.printPassengers()


# In[6]:


ic = InterCity("ICE64346")
ic.stepIn(p1)
ic.stepIn(p2)
ic.stepIn(p3)
ic.printPassengers()
ic.stepOut(2)
ic.printPassengers()

