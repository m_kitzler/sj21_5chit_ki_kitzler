#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import seaborn as sb
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn import metrics
from sklearn.metrics import plot_confusion_matrix
from sklearn.model_selection import GridSearchCV
import sklearn
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn import svm
from sklearn.svm import SVC
from sklearn.metrics import classification_report, confusion_matrix


# In[2]:


df = pd.read_csv("diabetes.csv")


# In[3]:


# Festlegen der Parameter
x = df[['Pregnancies', 'Glucose', 'BloodPressure', 'SkinThickness', 'Insulin', 'BMI', 'DiabetesPedigreeFunction', 'Age']]
y = df["Outcome"]
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=.3, random_state = 1)

# Von GridSearchDemo
param_grid = {
    'bootstrap': [True],
    'max_depth': [80, 90, 100],
    'max_features': [2, 3],
    'min_samples_leaf': [3, 4, 5],
    'min_samples_split': [8, 10, 12],
    'n_estimators': [100, 150, 200]
}


# In[4]:


clf2 = RandomForestClassifier(random_state=1)
grid_search = GridSearchCV(estimator = clf2, param_grid = param_grid, n_jobs = -1, verbose = 2)
grid_search.fit(x_train, y_train)


# In[5]:


print("Besten Parameter:")
print(grid_search.best_params_)
print("\nBeste Genauigkeit:")
print(grid_search.best_score_)


# In[6]:


param_grid = [
  {'C': [0.001, 0.01, 0.1, 1, 10], 'gamma': [0.001, 0.01, 0.1, 1], 'kernel': ['rbf', 'linear', 'sigmoid']}
 ]


# In[7]:


svc = SVC(random_state=1)
# Suchen starten
grid_search_svc = GridSearchCV(svc, param_grid)
grid_search_svc.fit(x_train,y_train)


# In[8]:


print("Besten Parameter:")
print(grid_search_svc.best_params_)
print("\nBeste Genauigkeit:")
print(grid_search_svc.best_score_)


# In[9]:


#Median für alle Spalten ausrechnen, wo die Outcome-Spalte 0 ist
medianForZeros = df.loc[df["Outcome"]==0].median()
#Median für alle Spalten ausrechnen, wo die Outcome-Spalte 1 ist
medianForOnes = df.loc[df["Outcome"]==1].median()


# In[10]:


# alle 0 Werte durch den Median ersetzen mit berücksichtigung der outcome spalte
def replace_function(row):
    idx = 0
    rownames = ['Pregnancies', 'Glucose', 'BloodPressure', 'SkinThickness', 'Insulin', 'BMI', 'DiabetesPedigreeFunction', 'Age', 'Outcome']
    newrow = row
    for value in row: # der eigentliche wert der spalten in der zeile wo wir uns gerade befinden
        rowname = rownames[idx] #für jede spalte
        idx+=1
        #überall ersetzen ausser bei Pregnancies (0 Kinder ist hier ein wahrer Wert) und Outcome
        if value == 0 and rowname != 'Pregnancies' and rowname != 'Outcome':
            if row['Outcome'] == 0:
                newrow[rowname] = medianForZeros[rowname] #ersetzen des 0 Werts der Spalte durch den Median der richtigen Spalte, mit berücksichtigung ob Outcome 0 ist
            elif row['Outcome'] == 1:
                newrow[rowname] = medianForOnes[rowname] #ersetzen des 0 Werts der Spalte durch den Median der richtigen Spalte, mit berücksichtigung ob Outcome 1 ist
    return newrow

#in neues, eigenes DF speichern
newDF = df.apply(replace_function, axis=1)


# # Neuberechnung mit überarbeiteten Werten

# In[11]:


x2 = newDF[['Pregnancies', 'Glucose', 'BloodPressure', 'SkinThickness', 'Insulin', 'BMI', 'DiabetesPedigreeFunction', 'Age']]
y2 = newDF["Outcome"]
x2_train, x2_test, y2_train, y2_test = train_test_split(x2, y2, test_size=.3, random_state = 1)

param_grid = {
    'bootstrap': [True],
    'max_depth': [80, 90, 100],
    'max_features': [2, 3],
    'min_samples_leaf': [3, 4, 5],
    'min_samples_split': [8, 10, 12],
    'n_estimators': [100, 150, 200]
}


# In[12]:


grid_search = GridSearchCV(estimator = clf2, param_grid = param_grid, n_jobs = -1, verbose = 2)
grid_search.fit(x2_train, y2_train)


# In[13]:


print("Besten Parameter:")
print(grid_search.best_params_)
print("\nBeste Genauigkeit:")
print(grid_search.best_score_)


# In[14]:


param_grid = [
  {'C': [0.001, 0.01, 0.1, 1, 10], 'gamma': [0.001, 0.01, 0.1, 1], 'kernel': ['rbf', 'linear', 'sigmoid']}
 ]


# In[15]:


svc = SVC(random_state=1)
grid_search_svc = GridSearchCV(svc, param_grid)
grid_search_svc.fit(x2_train,y2_train)


# In[16]:


print("Besten Parameter:")
print(grid_search_svc.best_params_)
print("\nBeste Genauigkeit:")
print(grid_search_svc.best_score_)


# # Vergleich mit 0-Werten und ohne 0-Werten
# Das DF ohne 0-Werte (newDF) hat durchaus eine höhere accuracy, sowohl RFC als auch SVM.  
# Accuracy:
# 
# 
# |     |RFC |SVM |
# |-----|----|----|
# |with0|0.78|0.77|
# |no0  |0.90|0.86|

# In[17]:


clf = RandomForestClassifier(max_depth=80, n_estimators=100, random_state=1)
clf.fit(x2_train,y2_train)
y2_pred_rfc=clf.predict(x2_test)

print("Accuracy:",metrics.accuracy_score(y2_test, y2_pred_rfc))


# nur 2 Prozent unter der höchst erreichbaren accuracy

# In[18]:


svclassifier = SVC(kernel="rbf", random_state=1) 

svclassifier.fit(x2_train, y2_train)
y2_pred_svm = svclassifier.predict(x2_test)

print("Accuracy:",metrics.accuracy_score(y2_test, y2_pred_svm))


# nur 2 Prozent unter der höchst erreichbaren accuracy

# In[19]:


print(confusion_matrix(y2_test, y2_pred_rfc, labels=[1,0]))

plot_confusion_matrix(clf, x2_test, y2_test, labels=[1,0], cmap="Blues") 
plt.show()


# In[20]:


print(confusion_matrix(y2_test, y2_pred_svm, labels=[1,0]))

plot_confusion_matrix(svclassifier, x2_test, y2_test, labels=[1,0], cmap="Blues") 
plt.show()

