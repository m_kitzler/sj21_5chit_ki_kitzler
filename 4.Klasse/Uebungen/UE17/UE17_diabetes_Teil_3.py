#!/usr/bin/env python
# coding: utf-8

# # 17.3.1
# \#                  | qualitatives Merkmal | quantitatives Merkmal
# ------------------- | :------------------: | :-------------------:
# Körpergewicht       |  | X |
# Autobahnbezeichnung | X |  |
# Lieblingsfarbe      | X |  |
# Taschengeld         |  | X |
# Hausnummer          | X |  |

# # 17.3.2
# Alle Spalten bis auf country und continent sind quantitativ.

# # 17.3.3

# In[1]:


import pandas as pd
import numpy as np
df = pd.read_csv('diabetes.csv')


# In[42]:


df['Demo'] = df['Demo'].apply(lambda x: np.random.choice(['A', 'B', 'C'])).astype('category')
df.head()


# In[43]:


df['Demo'].describe()


# In[44]:


df.info()


# # 17.3.4

# [siehe hier](https://towardsdatascience.com/methods-for-dealing-with-imbalanced-data-5b761be45a18)
