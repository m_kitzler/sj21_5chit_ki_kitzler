#!/usr/bin/env python
# coding: utf-8

# # Übung 12 - Pandas Recap 3
# 
# Arbeiten Sie nachfolgenden Aufgabenstellungen durch und dokumentieren Sie, wenn notwendig, ihre Erkenntnisse. 

# In[1]:


import pandas as pd


# ## Task 12.1
# 
# Erstellen Sie a) das DataFrame `person_df` mit folgendem Inhalt:
# 
# <table>
#     <tr><th></th><th>Gewicht</th><th>Größe</th></tr>
#      <tr><td>Henry</td><td>75</td><td>179</td></tr>
#     <tr><td>Sarah</td><td>68</td><td>165</td></tr>
#     <tr><td>Elke</td><td>68</td><td>172</td></tr>
#     <tr><td>Susi</td><td>55</td><td>164</td></tr>
#     <tr><td>Vera</td><td>58</td><td>160</td></tr>
#     <tr><td>Toni</td><td>99</td><td>189</td></tr>
#     <tr><td>Maria</td><td>68</td><td>176</td></tr>
#     <tr><td>Chris</td><td>60</td><td>175</td></tr>    
# </table>
# 

# In[2]:


#a)
person_df = pd.DataFrame({'Gewicht': [75, 68, 68, 55, 58, 99, 68, 60], 'Größe': [179, 165, 172, 164, 160, 189, 176, 175]}, index=['Henry', 'Sarah', 'Elke', 'Susi', 'Vera', 'Toni', 'Maria', 'Chris'])
person_df


# Der sog. *Body Mass Index* [1] berechnet sich durch Körpermasse [kg] / Körpergröß [m]². Berechnen Sie b) den BMI für alle Personen des DataFrames `person_df` und geben Sie ausschließlich jene aus, deren BMI > 20 und < 25 ist. 
# 
# **Hinweis**: Erstellen Sie KEINE neue Spalte im DataFrame. Es ist ausschließlich folgendes Ergebnis in der Zelle auszugeben:
# 
# ```Python
# Henry    23.407509 
# Sarah    24.977043 
# Elke     22.985398  
# Susi     20.449137  
# Vera     22.656250  
# Maria    21.952479
# dtype: float64
# ``` 
# [1] https://de.wikipedia.org/wiki/Body-Mass-Index 

# In[3]:


# b - klassische Ansatz ohne apply & lambda)
temp_df = person_df['Gewicht']/pow((person_df['Größe']/100), 2)
for index, value in temp_df.iteritems():
    if value > 20 and value < 25:
        print(f'{index}\t{value}')


# Nachdem die Berechnung erfolgreich war, fügen Sie c) die ermittelten Werte (je Person) dem DataFrame `person_df` hinzu. Als Spaltenname ist *BMI* zu wählen.

# In[4]:


#c)
person_df['BMI'] = temp_df
person_df


# Geben Sie d) das erzeugte DataFrame absteigend sortiert nach dem BMI aus.

# In[5]:


#d)
person_df.sort_values('BMI', ascending=False)


# ## Task 12.2
# 
# Laden Sie das bereitgestellte Dataset *parks.csv* und verschaffen Sie sich einen Überblick über dessen Aufbau bzw. Inhalt.

# In[6]:


parks_df = pd.read_csv('parks.csv')
parks_df.head()


# a) Geben Sie den Park mit der ID 9 aus:

# In[7]:


#a)
parks_df.iloc[9]


# b) Geben Sie Filme mit der ID 3, 12 und 24 aus:

# In[8]:


#b)
parks_df.iloc[[3, 12, 24]]


# c) Wie ist das DataFrame `park_df` zu ändern, dass die Abfrage `park_df.loc['BIBE']` durchläuft und somit folgendes Ergebnis liefert:
# 
# ```Python
# Park Name    Big Bend National Park
# State                            TX
# Acres                        801163
# Latitude                      29.25
# Longitude                   -103.25
# Name: BIBE, dtype: object
# ```

# In[9]:


#c)
new_parks_df = parks_df.set_index('Park Code')


# In[10]:


new_parks_df.loc['BIBE']


# d) Geben Sie die ersten drei sowie den 4., 5. und 6 Park aus (zwei separate Anfragen mit `iloc`):

# In[11]:


#d)
print(parks_df.iloc[:3])
print(parks_df.iloc[3:6])


# e) Gesucht ist folgende Ausgabe der Spalte *Park Code*:
# 
# ```Python
# 0    ACAD
# 1    ARCH
# 2    BADL
# Name: Park Code, dtype: object
# ```

# In[24]:


#e)
parks_df['Park Code'][0:3]


# Spaltennamen mit Leerzeichen (und Großbuchstaben) sind eine potenzielle Fehlerquelle, die es zu eliminieren gilt. Ändern Sie f) die Spaltennamen durch den Einsatz von `replace(...)` und `lower(...)` in einer *List Comprehension*. **Wichtig**: Die Liste mit den neuen Spaltennamen ist in der *List Comprehension* zu erstellen. Warum wir eine Liste benötigen, ist durch das Property *columns* von *DataFrame* definiert. `new_column_names` gestaltet sich nach Abarbeitung der *List Comprehension* wie folgt:
# 
# ```Python
# ['parkcode', 'parkname', 'state', 'acres', 'latitude', 'longitude']
# ```

# In[31]:


#f) Neue Spaltennamen
new_column_names = []

for col in parks_df.columns:
    new_column_names.append(col.replace(' ','').lower())
    
new_column_names


# Selektieren Sie g) den Parknamen und den Bundestaat der ersten 3 Zeilen im *DataFrame*.

# In[35]:


#g)
parks_df[['Park Name', 'State']][:3]


# h) Worin unterscheiden Sie diese beiden Abfragen und was wäre eine logische Erklärung dafür?
# - `park_df.iloc[2]`
# - `park_df.iloc[[2]]`

# In[40]:


# h)
#wenn ein einziger Eintrag aufgerufen wird werden die Eigenschaften dessen ausgegeben
print(parks_df.iloc[2])

#hier wird Eintrag jedoch als Liste zurückgegeben, da [2] eine Liste ist und pandas dadurch mehrere Indexes erwartet
parks_df.iloc[[2]]


# i) Welche Parks befinden sich im Bundesstaat Utah (UT)?

# In[43]:


#i)
parks_df[parks_df['State'] == 'UT']


# j) Welche Parks erfüllen folgende Bedingung? 
# - latitude > 60 oder acres > 1000000

# In[47]:


#j)
parks_df[(parks_df['Latitude'] > 60) | (parks_df['Acres'] > 1000000)]


# k) Finden Sie alle Parks, die sich in den Bundesstaaten *WA*, *OR* und *CA* befinden. Verwenden Sie hierzu `isin()` (https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.isin.html?highlight=isin#pandas.DataFrame.isin) 

# In[48]:


#k)
parks_df[parks_df['State'].isin(['WA', 'OR', 'CA'])]

