#!/usr/bin/env python
# coding: utf-8

# # UE 13 - Pandas Datenaufbereitung
# 
# Diese Abschnitt beschäftigt sich schwerpunktmäßig mit den Pandas-Methoden `map()`, `apply()` und `applymap()`. 
# 
# > **Hinweis**: Fügen Sie neue Pandas-Befehle dem FactSheet.pdf hinzu.
# 
# Als Übungsgrundlage dient das *Iris Data Set* (Quelle: https://archive.ics.uci.edu/ml/datasets/Iris). Hierbei handelt es sich um einen Datensatz, der drei Irisarten (Iris setosa, Iris virginica und Iris versicolor) - also Blüten - unterscheidet.

# In[8]:


import pandas as pd


# ## 13.0 - Sklearn-Installation
# 
# Installieren Sie *Scikit Learn* (Sklearn) via *Anaconda Prompt* (Quelle: https://sklearn.org/install.html). Scikit ist eine frei Python-Bibliothek zum maschinellen Lernen, die auch gleich einige Datasets (Quelle: https://sklearn.org/modules/classes.html#module-sklearn.datasets) bereitstellt.

# ## 13.1 - Iris-Dataset laden
# Laden Sie das Iris Dataset aus dem Scikit Learn-Paket. Hierzu stellt Scikit mit `load_iris()` eine eigene Methode bereit. Bevor Sie das Dokument *Some notes to the iris dataset* (siehe htl.boxtree.at/lehre) durchnehmen, begründen Sie, warum a) `iris.shape` dem Interprete nicht schmeckt?
# 
# Nehmen Sie b) das oben erwähnte Dokument durch und probieren Sie sich an der Variable `iris`.

# In[3]:


import sklearn
from sklearn import datasets

iris = datasets.load_iris()
iris.shape
# Your Code...


# In[7]:


# Geht nicht, da iris kein DataFrame ist
iris


# ## 13.2 - DataFrame erstellen
# 
# Erstellen Sie a) das DataFrame `df_data` und geben Sie die ersten 5 Zeilen aus. Gefordert ist folgender Aufbau:
# 
# ```Python
#        sepal length (cm) 	sepal width (cm) 	petal length (cm) 	petal width (cm)
#     0 	   5.1 	                 3.5 	              1.4 	             0.2
#     1 	   4.9 	                 3.0 	              1.4 	             0.2
#     2 	   4.7 	                 3.2 	              1.3 	             0.2
#     3 	   4.6 	                 3.1 	              1.5 	             0.2
#     4 	   5.0 	                 3.6 	              1.4 	             0.2
# ```

# In[16]:


df_data = pd.DataFrame(iris["data"], columns=iris["feature_names"])
df_data.head()


# Fügen Sie b) dem DataFrame `df_data` die Spalte *Species* hinzu und erstellen Sie ein neues DataFrame mit dem Namen `iris_df`. Das gesuchte Ergebnis:
# 
# ```Python
#        sepal length (cm) 	sepal width (cm) 	petal length (cm) 	petal width (cm)       Species
#     0 	   5.1 	                 3.5 	              1.4 	             0.2               0
#     1 	   4.9 	                 3.0 	              1.4 	             0.2               0
#     2 	   4.7 	                 3.2 	              1.3 	             0.2               0
#     3 	   4.6 	                 3.1 	              1.5 	             0.2               0
#     4 	   5.0 	                 3.6 	              1.4 	             0.2               0
# ```
# 
# 

# In[21]:


df_data["Species"] = iris["target"]
df_data.head()


# c) Verschaffen Sie sich einen Überblick, indem Sie folgende Fragen beantworten, und zwar auf Code-Eben:
# 
# - Über wie viele Zeilen verfügt das DataFrame `iris_df`?
# - Wie viele unterschiedliche Arten (Species) gibt es und wie viele umfasst die jeweilige Art?
# - Wie viele Zellen weisen `nan` auf?
# - Beurteilen Sie, ob die Mittelwertbildung der Spalte 'Species' Sinn ergibt.
# - Finden Sie heraus, ob eine Korrelation zwischen einzelen Features (Sepal length,..., Petal width) besteht.

# In[44]:


print("1)")
print(df_data.shape[0])

print("\n2)")
print(df_data["Species"].unique())

print("\n3)")
print(df_data.isna().sum())

# 4)
# Nein, da jede Zahl eine ganz andere Spezies is

print("\n5)")
print(df_data.corr())
# nicht direkt, aber petal length und width haben schon eine gewisse Abhängigkeit


# ## 13.3 - apply, applymap und map
# 
# Arbeiten Sie das Tutorial https://towardsdatascience.com/introduction-to-pandas-apply-applymap-and-map-5d3e044e93ff  durch. Abgabe der Code-Beispiele ist nicht notwendig.

# ## 13.4 Data preparation with apply, applymap or map
# 
# Überschreiben Sie a) die Spalte *Species*, wobei folgende Zuordnung gilt:
# 
# - 0 => SET
# - 1 => VER
# - 2 => VIR
# 
# Gesuchte Ergebnis:
# ```Python
#        sepal length (cm) 	sepal width (cm) 	petal length (cm) 	petal width (cm)       Species
#     0 	   5.1 	                 3.5 	              1.4 	             0.2               SET
#     1 	   4.9 	                 3.0 	              1.4 	             0.2               SET
#     2 	   4.7 	                 3.2 	              1.3 	             0.2               SET
#     3 	   4.6 	                 3.1 	              1.5 	             0.2               SET
#     4 	   5.0 	                 3.6 	              1.4 	             0.2               SET
# ```

# In[48]:


df_data["Species"] = df_data["Species"].map({0: "SET", 1: "VER", 2: "VIR"})


# In[65]:


df_data


# Erstellen Sie b) die neue Spalte `wide petal`, die das Ergebnis folgender Bedingung enhält:
# 
# Wenn `petal width (cm) >= 1.3` ist, dann soll die Zelle der jeweiligen Zeile den Wert 1 aufweisen, sonst 0. Setzen Sie eine `lambda`-Expression ein.

# In[63]:


df_data["wide pedal"] = df_data["petal width (cm)"].apply(lambda x: 1 if x >= 1.3 else 0)


# In[64]:


df_data


# Ermitteln Sie c) die *petal area* (Petal-Fläche) und speichern Sie diese in der neu zu erstellenden Spalte *petal area*. Setzen Sie eine `lambda`-Expression ein. 

# In[119]:


df_data["pedal area"] = df_data.apply(lambda x: x["petal length (cm)"] * x["petal width (cm)"], axis=1)


# In[120]:


df_data


# Logarithmieren Sie d) alle jene Zellen, die vom Typ `float`sind. Verwenden Sie `np.log()`. `lambda` is still your friend!

# In[123]:


import numpy as np


# In[131]:


df_data.applymap(lambda x: np.log(x) if type(x) == float else x)

