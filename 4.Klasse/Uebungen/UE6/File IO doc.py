#!/usr/bin/env python
# coding: utf-8

# In[16]:


f = open('workfile', 'w')  #r read, w write (will delete existing file), a append, r+ read/write
f.write('Hello World')
f.close()
f = open('workfile', 'r')
print(f.read())
f.close()


# In[32]:


with open('workfile', 'r+b') as f:  #open in binary mode and auto-closes file after
    f.read(11)
    val = f.write(bytes('\nTest', 'utf-8'))  #returns written bytes
    print(val)
    print(f.tell())  #current position
    f.seek(0)  #set position; second param:0 start of file, 1 current pos, 2 end of file (default 0)
    print(f.readlines())
f.closed

