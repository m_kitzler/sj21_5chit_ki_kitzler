#!/usr/bin/env python
# coding: utf-8

# In[29]:


with open('shopping_cart.txt', 'w') as f:
    f.write(('#Nr;Menge;Einzelpreis;Bezeichnung\n'
        '4711;2;100.20;Riesenschultüte\n'
        '0815;10;2.33;Kaugummi\n'
        '9365;2;5.11;Gemüse\n'
        '9999;55;100000.00;Gold\n'
        '8365;2;1.99;Karotte\n'
        '2856;1;0.50;Stein\n'
        '4175;5;6.99;Brot\n'
        '1263;2;0.99;Wasser 0.5L\n'
        '5532;3;10.99;Smart Water\n'
        '7391;5;5.23;Lebensmittel fraglicher Qualität\n'))


# In[33]:


from decimal import *
with open('shopping_cart2.txt', 'w') as fw:
    fw.write('#Nr;Gesamtpreis;Bezeichnung\n')
    with open('shopping_cart.txt', 'r') as fr:
        fr.readline()
        read = fr.readline()
        while not read == '':
            read = read.split(';')
            ret = read[0] + ';'
            ret += f'{Decimal(read[1]) * Decimal(read[2])};{read[3]}'
            fw.write(ret)
            read = fr.readline()


# In[35]:


from decimal import *
with open('shopping_cart.txt', 'r') as f1:
    with open('shopping_cart2.txt', 'r') as f2:
        f1.readline()
        f2.readline()
        read1 = f1.readline()
        read2 = f2.readline()
        while not read1 == '':
            read1 = read1.split(';')
            read2 = read2.split(';')
            assert Decimal(read1[1]) * Decimal(read1[2]) == Decimal(read2[1])
            read1 = f1.readline()
            read2 = f2.readline()

