#!/usr/bin/env python
# coding: utf-8

# In[1]:


from PIL import Image, ImageDraw
import face_recognition 


# # 21.1)

# In[2]:


image = face_recognition.load_image_file("data/biden.jpg")


# In[3]:


face_locations = face_recognition.face_locations(image)
print("I found {} face(s) in this photograph.".format(len(face_locations)))


# In[4]:


for face_location in face_locations:
    top, right, bottom, left = face_location
    face_image = image[top:bottom, left:right]
    pil_image = Image.fromarray(face_image)
    pil_image.show()
    print("A face is located at pixel location Top: {}, Left: {}, Bottom: {}, Right: {}".format(top, left, bottom, right))


# # 21.2)

# In[5]:


image = face_recognition.load_image_file("data/pexels-photo-3184419.jpeg")
face_locations = face_recognition.face_locations(image)
print("Auf dem Bild sind {} Gesichter zu sehen.".format(len(face_locations)))
print(face_locations)


# # 21.3)

# In[6]:


for face_location in face_locations:
    top, right, bottom, left = face_location
    face_image = image[top:bottom, left:right]
    pil_image = Image.fromarray(face_image)
    pil_image.show()


# # 21.4)

# In[17]:


image = face_recognition.load_image_file("data/biden.jpg")
face_landmarks = face_recognition.face_landmarks(image)
pil_image = Image.fromarray(image)
for face_landmark in face_landmarks:
    d = ImageDraw.Draw(pil_image, 'RGBA')

    # Make the eyebrows into a nightmare
    d.polygon(face_landmark['left_eyebrow'], fill=(68, 54, 39, 128))
    d.polygon(face_landmark['right_eyebrow'], fill=(68, 54, 39, 128))
    d.line(face_landmark['left_eyebrow'], fill=(68, 54, 39, 150), width=5)
    d.line(face_landmark['right_eyebrow'], fill=(68, 54, 39, 150), width=5)

    # Gloss the lips
    d.polygon(face_landmark['top_lip'], fill=(150, 0, 0, 128))
    d.polygon(face_landmark['bottom_lip'], fill=(150, 0, 0, 128))
    d.line(face_landmark['top_lip'], fill=(150, 0, 0, 64), width=8)
    d.line(face_landmark['bottom_lip'], fill=(150, 0, 0, 64), width=8)

    # Sparkle the eyes
    d.polygon(face_landmark['left_eye'], fill=(255, 255, 255, 30))
    d.polygon(face_landmark['right_eye'], fill=(255, 255, 255, 30))

    # Apply some eyeliner
    d.line(face_landmark['left_eye'] + [face_landmark['left_eye'][0]], fill=(0, 0, 0, 110), width=6)
    d.line(face_landmark['right_eye'] + [face_landmark['right_eye'][0]], fill=(0, 0, 0, 110), width=6)

    pil_image.show()

