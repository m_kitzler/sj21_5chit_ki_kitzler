#!/usr/bin/env python
# coding: utf-8

# In[130]:


import pandas as pd
import numpy
import tensorflow as tf
import matplotlib.pyplot as plt
from tensorflow.keras.datasets import mnist
from tensorflow.keras.layers import *
from tensorflow.keras.models import load_model
from tensorflow.keras.utils import to_categorical
import numpy as np
from PIL import Image
import glob


# # MNIST 1

# In[67]:


#(x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data(path="mnist.npz")
(x_train, y_train), (x_test, y_test) = mnist.load_data()
x_train.shape


# In[6]:


pd.DataFrame(numpy.array(x_train).reshape(60000,784))


# In[3]:


plt.imshow(x_train[np.where(y_train == 5)[0][0]], cmap="gray_r")


# In[5]:


print(f"Die Zahl ist: {y_train[0]}")


# ## 18.4)
# Es werden 10 Knoten für jede der möglichen Zahlen gebraucht

# ## 18.5)

# In[46]:


model = tf.keras.Sequential()
model.add(Dense(512, activation="relu", input_shape=(784,)))
model.add(Dense(10, activation="softmax"))

model.compile(optimizer="rmsprop", loss="categorical_crossentropy", metrics=["accuracy"])


# ## 18.6)

# In[68]:


train_images = x_train.reshape(60000,784)
test_images = x_test.reshape(10000,784)
train_labels = to_categorical(y_train)
test_labels = to_categorical(y_test)


# In[7]:


model.fit(train_images, train_labels, epochs=5, batch_size=128)


# ## 18.7)

# In[8]:


model.evaluate(test_images, test_labels)


# # MNIST 2
# ## 18.2.1)

# In[10]:


model_history = model.fit(train_images, train_labels, epochs=15, batch_size=128)


# In[11]:


plt.plot(model_history.history["accuracy"], "b", label="Training")
plt.ylim(0.9, 1)
plt.title("Korrektklassifizierungsrate Training")
plt.xlabel("Epochen")
plt.ylabel("Korrektklassifizierungsrate")
plt.legend()
plt.show()


# ## 18.2.2)

# In[49]:


model_history = model.fit(train_images, train_labels, epochs=15, batch_size=128, validation_data=(test_images, test_labels))


# In[13]:


plt.plot(model_history.history["accuracy"], "b", label="Training")
plt.plot(model_history.history["val_accuracy"], "r", label="Validierung")
plt.title("Korrektklassifizierungsrate Training")
plt.xlabel("Epochen")
plt.ylabel("Korrektklassifizierungsrate")
plt.legend()
plt.show()


# In[74]:


mean = np.mean(model_history.history["val_accuracy"])
max = np.max(model_history.history["val_accuracy"])
print(f"Mean Accuracy: {mean}")
print(f"Max Accuracy: {max}")


# # MNIST 3
# ## 18.3.1)

# In[50]:


#crossX = np.apply_along_axis(lambda a: np.argmax(a), 1, model.predict(test_images))
#crossY = np.apply_along_axis(lambda a: np.argmax(a), 1, test_labels)
crossX = model.predict(test_images).argmax(axis=1)
crossY = test_labels.argmax(axis=1)
pd.crosstab(crossX, crossY, rownames=["actual"], colnames=["predicted"])


# In[38]:


test_labels.argmax(axis=1)


# ## 18.3.2)

# In[69]:


train_images_norm = train_images / 255.0
test_images_norm = test_images / 255.0


# In[67]:


model_history = model.fit(train_images_norm, train_labels, epochs=15, batch_size=128, validation_data=(test_images_norm, test_labels))


# In[68]:


mean = np.mean(model_history.history["val_accuracy"])
max = np.max(model_history.history["val_accuracy"])
print(f"Mean Accuracy: {mean}")
print(f"Max Accuracy: {max}")


# ## Non-normalized
# ### Epochs: 15, Batch: 128
# Mean Accuracy: 0.9669133345286052  
# Max Accuracy: 0.9746999740600586
# ## Normalized
# ### Epochs: 15, Batch: 128
# Mean Accuracy: 0.9780399998029073  
# Max Accuracy: 0.9828000068664551

# ## 18.3.3)

# In[43]:


model = tf.keras.Sequential()
model.add(Dense(512, activation="relu", input_shape=(784,)))
model.add(Dense(10, activation="softmax"))

model.compile(optimizer="rmsprop", loss="categorical_crossentropy", metrics=["accuracy"])


# In[44]:


model_history = model.fit(train_images_norm, train_labels, epochs=10, batch_size=1024, validation_data=(test_images_norm, test_labels))


# In[33]:


print(f"Mean Accuracy: {np.mean(model_history.history['val_accuracy'])}")
print(f"Max Accuracy: {np.max(model_history.history['val_accuracy'])}")
print(f"Mean Loss: {np.mean(model_history.history['val_loss'])}")
print(f"Max Loss: {np.max(model_history.history['val_loss'])}")


# ## epochs=
# ### 10
# Mean Accuracy: 0.9831199884414673  
# Max Accuracy: 0.983299970626831  
# Mean Loss: 0.15986983925104142  
# Max Loss: 0.1603856235742569
# ### 20
# Mean Accuracy: 0.9831500053405762  
# Max Accuracy: 0.9832000136375427  
# Mean Loss: 0.1583306223154068  
# Max Loss: 0.15942810475826263
# ### 50
# Mean Accuracy: 0.9832499957084656  
# Max Accuracy: 0.9833999872207642  
# Mean Loss: 0.15396952748298645  
# Max Loss: 0.15715792775154114
# 
# ## batch_size=
# ### 64
# Mean Accuracy: 0.9832399964332581  
# Max Accuracy: 0.9833999872207642  
# Mean Loss: 0.16125680059194564  
# Max Loss: 0.16228044033050537
# ### 1024
# Mean Accuracy: 0.9832000136375427  
# Max Accuracy: 0.9832000136375427  
# Mean Loss: 0.16235847473144532  
# Max Loss: 0.16244646906852722

# ## 18.3.4)

# In[45]:


model.evaluate(test_images_norm, test_labels)


# In[52]:


model.save("model")
del model


# In[53]:


model.evaluate(test_images_norm, test_labels)
# => model is not defined


# In[54]:


model = load_model("model")


# In[55]:


model.evaluate(test_images_norm, test_labels)


# # MNIST 4

# In[22]:


img = Image.open("numbers/5.png").convert("L")
#img = np.asarray(img)
img


# In[131]:


own_images = []
own_labels = []
for f in glob.iglob("numbers/*"):
    own_images.append(np.asarray(Image.open(f).convert("L")))
    file = f.split("\\")
    own_labels.append(int(file[len(file) - 1][0]))
    
own_images = np.array(own_images).reshape(len(own_images), 784) / 255.0


# In[132]:


model = load_model("model")
pred = model.predict(own_images).argmax(axis=1)


# In[134]:


df = pd.DataFrame({'predicted':pred, 'actual':own_labels})
df

