#!/usr/bin/env python
# coding: utf-8

# In[29]:


import numpy as np
import pandas as pd
from tensorflow.keras import *
from tensorflow.keras.layers import *
from tensorflow.keras.datasets import boston_housing
from tensorflow.keras.utils import to_categorical
import matplotlib.pyplot as plt


# ## 19.1.1)

# In[30]:


(x_train, y_train), (x_test, y_test) = boston_housing.load_data()


# In[4]:


x_train.shape


# In[8]:


x_train[0]


# ## 19.1.2)

# In[32]:


model = Sequential()
model.add(Dense(64, activation="relu", input_shape=(x_train.shape[1],)))
model.add(Dense(64, activation="relu"))
model.add(Dense(1))

model.compile(optimizer="rmsprop", loss="mse", metrics=["mae"])


# In[48]:


model.fit(x_train, y_train, epochs=200, batch_size=1)


# In[49]:


model.evaluate(x_test, y_test)


# ## 80 epochs
# [35.2586784362793, 4.264451026916504]
# ## 120 epochs
# [29.7452335357666, 3.5835633277893066]
# ## 200 epochs
# [22.025327682495117, 3.366074800491333]

# ## 19.1.3)

# In[31]:


x_train -=  x_train.mean(axis=0)
x_train /= x_train.std(axis=0)
x_test -=  x_test.mean(axis=0)
x_test /= x_test.std(axis=0)


# In[33]:


model.fit(x_train, y_train, epochs=80, batch_size=1, verbose=0)


# In[34]:


model.evaluate(x_test, y_test)

