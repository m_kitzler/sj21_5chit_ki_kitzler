#!/usr/bin/env python
# coding: utf-8

# # NumPy - erste Schritte
# Bei **Numpy** (https://numpy.org/doc/stable/index.html) handelt es sich um ein Modul, welches grundlegende Datenstrukturen (mehrdimensionale Arrays und Matrizen) zur Verfügung stellt, die auch *Matplotlib*, *SciPy* und *Pandas* benutzt werden. Die Bezeichnung NumPy ist eine Abkürzung für "Numerical Python" (Numerisches Python). Was damit gemeint ist, werden die nachfolgenden Übung verdeutlichen. 
# 
# Wissenswert ist, dass ein großer Teil von NumPy in C geschrieben worden ist. Die kompilierten mathematischen und numerischen Funktionen und Funktionalitäten werden somit mit "größter" Geschwindigkeit ausgeführt..

# In[1]:


import numpy as np


# In[2]:


#Wir definieren eine Python-Liste mit Temperaturwerten
c_values = [20.1, 20.8, 21.9, 22.5, 22.7]

c = np.array(c_values)
print(c, type(c))


# In[6]:


#Berechnen sie mit Python die Temperatur in Grad Fahrenheit: ° C * 1.8 + 32
f_values1 = []
for i in range(0, len(c_values)):
    f_values1.append(c_values[i] * 1.8 + 32)
print(f_values1)

#Andere Art
f_values2 = [val * 1.8 + 32 for val in c_values]
print(f_values2)


# In[9]:


#Mit NumPy
print(c * 1.8 + 32)  #NumPy übernimmt das Iterieren über die Elemente
#c wird nicht manipuliert
print(c)


# In[10]:


#Genau genommen ist c nur eine Instanz der ndarray-Klasse
type(c)


# ## Grafische Darstellung der Werte
# Um zu demonstrieren, wie schnell mithilfe von *Matplotlib* ansprechende Diagramme erstellt werden können, geben wir die oben erstellten Temperaturwerte mit diesem Modul aus. Eine detaillierte Besprechung des Moduls erfolgt später. 

# In[12]:


get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib.pyplot as plt

plt.plot(c)
plt.show()


# Die Werte des Arrays `c` dienen als Grundlage für die Ordinate (y-Achse). Die Indizes sind der Abszisse (x-Achse) zu entnehmen. 

# ## Weitere Funktionen zum Erzeugen von Arrays
# NumPy bietet Funktionen, um Intervalle mit Werten zu erzeugen, deren abstände gleichmäßig verteilt sind. 
# 
# 
# ### arange
# **arange** liefert gleichmäßig verteilte Werte innerhalb eines Intervalls zurück. Mit Interger-Werte ist diese Funktion Quasi-Äquivalent" mit der Python-Funktion `range`

# In[32]:


#Inklusive Start und exklusive Stopp
a = np.arange(0, 7)
print(a)

x = range(0, 7)
print(list(x))

a = np.arange(0, 20, 5)
print(a)

x = range(0, 20, 5)
print(list(x))

a = np.arange(7.2)  #Wenn der Wert genau matcht, ist er nicht mehr dabei
print(a)

x = range(7)  #Python range kann keine Kommazahlen
print(list(x))

#Achtung: "When using a non-integer step the result will often not be consistent" -NumPy Doc
a = np.arange(0.5, 6.1, 0.8)
print(a)

a = np.arange(0.5, 6.1, 0.8, int)  #Rundet 0.8 auf 1 und inkrementiert dann erst
print(a)

a = np.arange(0.5, 6.1, 0.8, dtype='uint16')  #Angeben eines C-Wertes
print(a)


# ### Nulldimensionale Arrays
# Nulldimensionale Arrays werden auch **Skalare** oder **0-D-Tensor** genannt. Hierbei handelt es sich um einen Tensor, der nur **einen Wert** enthält. Mit `ndim` erhält man die Dimension des Tensors, mit `dtype` den Typ des Arrays. Nachdem NumPy C-basiert ist, sind viel mehr Datentypen (z.B. int8, int16 etc.) möglich.

# In[39]:


x = np.array(42, dtype='int8')
print(x, type(x))
print('Die Dimension von x ist: ', x.ndim)
print('Die Dimension von x ist: ', np.ndim(x))
print('Der Typ des Arrays ist: ', x.dtype)


# ### Eindimensionales Array
# Ein Array von Zahlen wird als **Vektor** oder **1-D-Tensor** bezeichnet. Ein 1-D-Tensor besitzt genau 1 Achse.

# In[41]:


x = np.array([14, 6, 3, 12])
print(x, type(x))
print('Die Dimension von x ist: ', x.ndim)
print('Der Typ des Arrays ist: ', x.dtype)


# ### Zweidimensionale Arrays
# Ein Array von Vektoren nennt man **2-D-Tensor** oder **Matrix**. Eine Matrix besitzt 2 Achsen, die oft als *Zeilen* und *Spalten* bezeichnet werden. Die Inhalte der ersten Achse heißen Zeile, die Inhalte der zweitem Spalten. 

# In[45]:


x = np.array([[14, 6, 3, 12],
              [5, 78, 2, 67],
              [23, 72, 23, 34]])
print(x, type(x))
print('Die Dimension von x ist: ', x.ndim)
print('Der Typ des Arrays ist: ', x.dtype)


# > 3-D-Tensoren und höherdimensionale Tensoren sehen wir uns zu einem späteren Zeitpunkt an!

# ### Shape bzw. die Gestalt eines Arrays
# 
# Die Funktion `shape` liefert Informationen über die Gestalt eines Arrays. De Return-Wert ist ein Integer-Tupel. Vereinfacht ausgedrückt, ist liefert `shape` die Anzahl der Elemente pro Achse (Dimensionen). Im obigen Bsp. ist die Shape gleich **(3,5)**. Das bedeutet, dass wir **3 Zeilen** und **5 Spalten** haben. 

# In[46]:


print(x.shape)


# > Der Output von `shape`sagt auch etwas über die Reihenfolge, mit der die Indizes ausgeführt werden, aus, dh. zuerst die Zeilen, dann die Spalten (und dann gegebenenfalls noch weitere Dimensionen).

# Jetzt ein wenig *magic*: Mit `shape` kann man die Gestalt eines Arrays verändern.

# In[48]:


x.shape = (4, 3)  #Entspricht "reshape"
print(x)


# ## Matrizenmultiplikation und Skalarprodukt
# In englischen Texten wird häufig vom 'dot product' gesprochen. Mathematisch versteht man darunter das Skalarprodukt von zwei Vektoren. Um das Skalarprodukt zu berechnen, stellt NumPy die `dot`-Funktion zur Verfügung. **Hinweis**: Die `dot`-Funktion kann auch auf mehrdimensionale Arrays angewandt werden.

# In[50]:


x = np.array([3, -2])
y = np.array([-4, 1])

print(np.dot(x,y))
print(x.dot(y))

