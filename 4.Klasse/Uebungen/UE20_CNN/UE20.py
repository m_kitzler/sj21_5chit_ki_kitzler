#!/usr/bin/env python
# coding: utf-8

# # Numpy Warmup

# In[23]:


import numpy as np
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import *
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.datasets import mnist
from tensorflow.keras.preprocessing.image import *
import tensorflow.keras.backend as K
import matplotlib.pyplot as plt
import os
import sys
import pandas as pd
from sklearn.model_selection import train_test_split


# In[18]:


arr = np.arange(1, 10)
arr


# In[19]:


arr = arr.reshape(3,3)
arr


# In[20]:


arr = np.average(arr, axis=0)
arr


# # 20.1.1)
# Ein RGB-Bild würde eine Shape von (28, 28, 3) für jeden der drei Farbkanäle benötigen.

# # 20.1.2)

# In[21]:


(x_train, y_train), (x_test, y_test) = mnist.load_data()


# In[33]:


model = Sequential()
model.add(Conv2D(10, kernel_size=(3,3), activation="relu", input_shape=(28,28,1)))
model.add(Flatten())
model.add(Dense(10, activation="softmax"))

model.compile(loss="categorical_crossentropy", metrics=["accuracy"])
model.output_shape


# In[22]:


train_images = x_train.reshape((60000, 28, 28, 1))
train_labels = to_categorical(y_train)
test_images = x_test.reshape((10000, 28, 28, 1))
test_labels = to_categorical(y_test)


# In[25]:


model.fit(train_images, train_labels, epochs=10, batch_size=1000)


# In[26]:


model.evaluate(test_images, test_labels)


# # 20.1.3)

# Flatten wird benötigt um ein multidimensionales Array (z.B. Ergebnis vom Convolution-Layer) in ein eindimensionales umzuformen.

# # 20.1.4)

# In[42]:


data = K.eval(model.layers[0].weights[0])
for x in range(9):
    plt.imshow(data[:,:,:,x].reshape(3,3))
    plt.show()


# # 20.2.1)

# In[116]:


model = Sequential()
model.add(Conv2D(10, kernel_size=(3,3), activation="relu", input_shape=(28,28,1)))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Conv2D(10, kernel_size=(3,3), activation="relu"))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Conv2D(10, kernel_size=(3,3), activation="relu"))
model.add(Flatten())
model.add(Dense(10, activation="softmax"))

model.compile(optimizer="rmsprop", loss="categorical_crossentropy", metrics=["accuracy"])


# In[117]:


model.fit(train_images, train_labels, epochs=5, batch_size=128)


# In[118]:


model.evaluate(test_images, test_labels)


# 1 Conv2D & Pooling: 0.091 loss, 0.977 acc  
# 2 Conv2D & Pooling: 0.074 loss, 0.979 acc  
# 3 Conv2D & Pooling: 0.188 loss, 0.945 acc

# In[132]:


model = Sequential()
model.add(Conv2D(10, kernel_size=(3,3), activation="relu", input_shape=(28,28,1)))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Conv2D(10, kernel_size=(3,3), activation="relu"))
model.add(Flatten())
model.add(Dense(64, activation="relu"))
model.add(Dense(10, activation="softmax"))

model.compile(optimizer="rmsprop", loss="categorical_crossentropy", metrics=["accuracy"])


# In[133]:


model.fit(train_images, train_labels, epochs=5, batch_size=128)


# In[135]:


model.evaluate(test_images, test_labels)


# default: 0.074 loss, 0.979 acc  
# Dense64: 0.074 loss, 0.982 acc

# In[136]:


model.summary()


# # 20.2.3)
# Summary zählt die Layer und (nicht-)trainierbaren Parameter.  
# Für den Dense Layer werden die Parameter mit $Channels_{Output} * (Channels_{Input} + 1)$.  
# Für einen Conv2D Layer wird die Kernelsize noch mitberechnet: $Channels_{Output} * (Channels_{Input} * Kernel_{Height} * Kernel_{Width} + 1)$.

# # 20.3.1)

# In[11]:


data = pd.DataFrame(columns=["path", "label"])
i = 0
prog = 1
sys.stdout.write("[" + " " * 20 + "]\r")
sys.stdout.flush()
for root, dirs, files in os.walk("train/"):
    prt = len(files) / 20
    for file in files:
        data = data.append({"path": f"{root}{file}", "label": file.split(".")[0]}, ignore_index=True)
        i += 1
        if (i >= prt*prog):
            sys.stdout.write("[" + "=" * prog + "\r")
            sys.stdout.flush()
            prog += 1
    sys.stdout.write("\n")
    sys.stdout.flush()
print(f"done, {i} files")


# In[65]:


data.head()


# # 20.3.2)

# In[169]:


data.shape


# In[174]:


data["label"].describe()


# # 20.3.3)

# In[15]:


def show_image(path):
    plt.imshow(load_img(path))


# In[16]:


show_image("train/cat.0.jpg")


# # 20.3.4)

# In[9]:


model = Sequential()
model.add(Conv2D(32, kernel_size=(3,3), activation="relu"))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Conv2D(64, kernel_size=(3,3), activation="relu"))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Conv2D(128, kernel_size=(3,3), activation="relu"))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Conv2D(128, kernel_size=(3,3), activation="relu"))
model.add(MaxPooling2D(pool_size=(2,2)))

model.add(Flatten())
model.add(Dense(512, activation="relu"))
model.add(Dense(2, activation="softmax"))

model.compile(optimizer="rmsprop", loss="categorical_crossentropy", metrics=["accuracy"])


# In[12]:


data["path"] = data.apply(lambda x: x["path"].split("/")[1], axis=1)
train_df, validation_df = train_test_split(data, test_size=0.20, random_state=12)
train_df = train_df.reset_index(drop=True)
validation_df = validation_df.reset_index(drop=True)
print(train_df.shape)
print(validation_df.shape)


# In[13]:


train_datagen = ImageDataGenerator(rescale=1./255)
validation_datagen = ImageDataGenerator(rescale=1./255)
train_generator = train_datagen.flow_from_dataframe(
    dataframe = train_df,
    directory = "train",
    x_col = "path",
    y_col = "label",
    target_size = (150,150),
    batch_size = 200,
    class_mode = "categorical"
)

validation_generator = validation_datagen.flow_from_dataframe(
    dataframe = validation_df,
    directory = "train",
    x_col = "path",
    y_col = "label",
    target_size = (150,150),
    batch_size = 200,
    class_mode = "categorical"
)


# In[14]:


for data_batch, labels_batch in train_generator:
    print(data_batch.shape)
    print(labels_batch.shape)
    break


# In[15]:


history = model.fit( train_generator, steps_per_epoch = 100, validation_data = validation_generator, epochs=10)


# In[16]:


test_filenames = os.listdir("test1")
test_df = pd.DataFrame({
    'filename': test_filenames
})
test_df.shape[0] 


# In[17]:


test_gen = ImageDataGenerator(rescale=1./255)
test_generator = test_gen.flow_from_dataframe(
    test_df,
    directory="test1",
    x_col='filename',
    y_col=None,
    class_mode=None,
    target_size=(150,150),
    batch_size=32,
    shuffle=False
)


# In[18]:


test_generator.reset()
pred=model.predict(test_generator, verbose=1)


# In[19]:


pred


# In[20]:


pred_rounded = np.argmax(pred, axis=-1)
test_df["category"] = pred_rounded
test_df["category"] = test_df["category"].map({0: 'cat', 1: 'dog'})
test_df.head(10)


# In[21]:


sample_test = test_df.head(18)
sample_test.head()
plt.figure(figsize=(12, 24))
for index, row in sample_test.iterrows():
    filename = row['filename']
    category = row['category']
    img = load_img("test1/"+filename, target_size=(150,150))
    plt.subplot(6, 3, index+1)
    plt.imshow(img)
    plt.xlabel(f"{filename} ({category})")
plt.tight_layout()
plt.show()


# In[22]:


from tensorflow.python.client import device_lib
print(device_lib.list_local_devices())


# In[24]:


import tensorflow as tf
tf.config.experimental.list_physical_devices()


# In[25]:


model.save("model")

