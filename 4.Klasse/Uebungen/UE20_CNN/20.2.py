#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
import keras
from keras.models import Sequential, Model
from keras.layers import *
from keras.utils import to_categorical
from keras.datasets import mnist


# # 20.2.1)

# In[2]:


(x_train, y_train), (x_test, y_test) = mnist.load_data()


# In[3]:


train_images = x_train.reshape((60000, 28, 28, 1))
train_labels = to_categorical(y_train)
test_images = x_test.reshape((10000, 28, 28, 1))
test_labels = to_categorical(y_test)


# In[4]:


model = Sequential()
model.add(Conv2D(10, kernel_size=(3,3), activation="relu", input_shape=(28,28,1)))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Conv2D(10, kernel_size=(3,3), activation="relu"))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Conv2D(10, kernel_size=(3,3), activation="relu"))
model.add(Flatten())
model.add(Dense(10, activation="softmax"))

model.compile(optimizer="rmsprop", loss="categorical_crossentropy", metrics=["accuracy"])


# In[5]:


model.fit(train_images, train_labels, epochs=5, batch_size=128)


# In[6]:


model.evaluate(test_images, test_labels)


# 1 Conv2D & Pooling: 0.091 loss, 0.977 acc  
# 2 Conv2D & Pooling: 0.074 loss, 0.979 acc  
# 3 Conv2D & Pooling: 0.188 loss, 0.945 acc

# # 20.2.2)

# In[7]:


model = Sequential()
model.add(Conv2D(10, kernel_size=(3,3), activation="relu", input_shape=(28,28,1)))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Conv2D(10, kernel_size=(3,3), activation="relu"))
model.add(Flatten())
model.add(Dense(64, activation="relu"))
model.add(Dense(10, activation="softmax"))

model.compile(optimizer="rmsprop", loss="categorical_crossentropy", metrics=["accuracy"])


# In[8]:


model.fit(train_images, train_labels, epochs=5, batch_size=128)


# In[9]:


model.evaluate(test_images, test_labels)


# default: 0.074 loss, 0.979 acc  
# Dense64: 0.074 loss, 0.982 acc

# In[10]:


model.summary()


# # 20.2.3)
# Summary zählt die Layer und (nicht-)trainierbaren Parameter.  
# Für den Dense Layer werden die Parameter mit $Channels_{Output} * (Channels_{Input} + 1)$.  
# Für einen Conv2D Layer wird die Kernelsize noch mitberechnet: $Channels_{Output} * (Channels_{Input} * Kernel_{Height} * Kernel_{Width} + 1)$.
