#!/usr/bin/env python
# coding: utf-8

# In[9]:


import os
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import keras
from keras.models import Model
from keras.layers import *
from keras.utils import to_categorical
from keras.preprocessing.image import *
from keras.applications import MobileNet
from keras.applications.mobilenet import preprocess_input, decode_predictions
from keras.optimizers import Adam
from sklearn.model_selection import train_test_split


# # 20.4.1)

# In[3]:


model = MobileNet(weights='imagenet')


# In[47]:


def predictImg(imgPath):
    image = load_img(imgPath, target_size=(224, 224))
    plt.imshow(image)
    image = img_to_array(image)
    image = image.reshape((1, image.shape[0], image.shape[1], image.shape[2]))
    preds = model.predict(preprocess_input(image))
    decPreds = decode_predictions(preds, top=3)[0]
    print('Predicted:')
    for pred in decPreds:
        print(f'{pred[1]}: {round(pred[2] * 100, 2)}%')


# In[48]:


predictImg("data/example1.jpg")


# In[49]:


predictImg("data/example2.jpg")


# In[50]:


predictImg("data/example3.jpg")


# In[51]:


predictImg("data/example4.jpg")


# # 20.4.2)

# In[52]:


base_model=MobileNet(weights='imagenet',include_top=False) #imports the mobilenet model and discards the last 1000 neuron layer.

x=base_model.output
x=GlobalAveragePooling2D()(x)
x=Dense(1024,activation='relu')(x) #we add dense layers so that the model can learn more complex functions and classify for better results.
x=Dense(1024,activation='relu')(x) #dense layer 2
x=Dense(512,activation='relu')(x) #dense layer 3
preds=Dense(3,activation='softmax')(x) #final layer with softmax activation


# In[53]:


model=Model(inputs=base_model.input,outputs=preds)
#specify the inputs
#specify the outputs
#now a model has been created based on our architecture


# In[54]:


for layer in model.layers[:20]:
    layer.trainable=False
for layer in model.layers[20:]:
    layer.trainable=True


# In[55]:


train_datagen=ImageDataGenerator(preprocessing_function=preprocess_input) #included in our dependencies

train_generator=train_datagen.flow_from_directory(
    'data/train_gh/', # this is where you specify the path to the main data folder
    target_size=(224,224),
    color_mode='rgb',
    batch_size=32,
    class_mode='categorical',
    shuffle=True
)


# In[56]:


model.compile(optimizer='Adam',loss='categorical_crossentropy',metrics=['accuracy'])
# Adam optimizer
# loss function will be categorical cross entropy
# evaluation metric will be accuracy

step_size_train=train_generator.n//train_generator.batch_size
model.fit_generator(
    generator=train_generator,
    steps_per_epoch=step_size_train,
    epochs=5
)


# # 20.4.3)

# In[57]:


data = pd.DataFrame(columns=["path", "label"])
i = 0
prog = 1
sys.stdout.write("[" + " " * 20 + "]\r")
sys.stdout.flush()
for root, dirs, files in os.walk("data/test/"):
    prt = len(files) / 20
    for file in files:
        data = data.append({"path": f"{root}{file}", "label": file.split(".")[0]}, ignore_index=True)
        i += 1
        if (i >= prt*prog):
            sys.stdout.write("[" + "=" * prog + "\r")
            sys.stdout.flush()
            prog += 1
    sys.stdout.write("\n")
    sys.stdout.flush()
print(f"done, {i} files")


# In[58]:


data["path"] = data.apply(lambda x: x["path"].split("/")[-1], axis=1)


# In[84]:


test_datagen = ImageDataGenerator(rescale=1./255)

test_generator = test_datagen.flow_from_dataframe(
    data.sample(10),
    directory="data/test",
    x_col='path',
    y_col=None,
    class_mode=None,
    target_size=(224,224),
    color_mode='rgb',
    batch_size=32,
    shuffle=False
)


# In[85]:


test_generator.reset()
pred=model.predict(test_generator, verbose=1)


# In[86]:


sample_df = pd.DataFrame(test_generator.filenames, columns=["filename"])
pred_rounded = np.argmax(pred, axis=-1)
sample_df["category"] = pred_rounded
sample_df["category"] = sample_df["category"].map({0: 'cat', 1: 'dog', 2: 'horse'})
sample_df.head(10)


# In[87]:


plt.figure(figsize=(12, 24))
for index, row in sample_df.iterrows():
    filename = row['filename']
    category = row['category']
    img = load_img("data/test/"+filename, target_size=(150,150))
    plt.subplot(6, 3, index+1)
    plt.imshow(img)
    plt.xlabel(f"{filename} ({category})")
plt.tight_layout()
plt.show()

