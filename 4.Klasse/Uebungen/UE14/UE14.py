#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import sklearn
from sklearn import datasets
iris = datasets.load_iris()

from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn import metrics

# Create a classifier of type "RandomForest"
clf = RandomForestClassifier(max_depth=10, n_estimators=10, verbose=2)


# In[2]:


x = pd.DataFrame(iris['data'], columns=iris['feature_names'])
y = iris['target']

print(x.head())
print(y)


# In[3]:


# split the data in a "training set" and a "test set"
# 30% is the size of our "test set" to check the results of the predicted values
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=.3)

# "put" the data („Beispiele“ + „Antworten“) into the classifier
clf.fit(x_train, y_train)

# store the predicted values in a matrix with one column(y_pred)
y_pred = clf.predict(x_test)

# 14.2
print(y_pred)


# In[4]:


# 14.3
data = pd.DataFrame({'predicted': y_pred, 'actual': y_test})
data['correct'] = data.apply(lambda x: 1 if x['actual'] == x['predicted'] else 0, axis=1)
data.head()


# In[5]:


print('Accuracy:', data['correct'].sum() / len(data))


# `max_depth` definiert wieviele "Fragen" ein Baum stellt, wodurch er größer wird. Eingrößerer Baum verbraucht mehr Speicher, wird aber auch genauer.
# `n_estimators` definiert die Anzahl der Bäume.
