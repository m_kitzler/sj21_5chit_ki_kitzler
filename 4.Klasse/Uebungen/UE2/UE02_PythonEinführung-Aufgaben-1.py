#!/usr/bin/env python
# coding: utf-8

# # UE02 Python Einführung - Aufgaben 
# Arbeiten Sie nachfolgenden Aufgabenstellungen durch. 
# 
# >**Hinweis:** Abzugeben ist ein Python-File (Export siehe Menüeintrag `File > Download as`).

# ## Task 2.1.1 - Strings
# Beantworten Sie nachfolgende Frage bzw. schreiben Sie jenen Code, den es zur Erfüllung der jeweiligen Aufgabenstellung braucht:
# 1. Macht es einen Unterschied, ob man einfache oder doppelte Anführungsstriche verwendet? 
# 2. Erzeugen Sie mit <code>print(...)</code> folgende Ausgabe : **"Now, I'm able to use single quotes!"**
# 3. Geben Sie durch Verwendung von <code>x</code> und <code>y</code> die Zeichenkette **Hello, world!** mit <code>print(...)</code> aus.

# Antwort zu 1.: Sie müssen nur mit den gleichen Zeichen wieder enden.

# In[1]:


#2.
print('"Now, I\'m able to use single quotes!"')


# In[72]:


#3.
hello, world = "Hello", "World"
print(hello + ", " + world + "!")
print(f"{hello}, {world}!")


# ## Task 2.1.2 - Indexbasierte String-Manipulation
# Nehmen Sie vorher https://docs.python.org/3.8/tutorial/introduction.html#strings durch, und zwar ab der Textstelle "*Strings can be indexed...*" Als Bearbeitungsgrundlage dient <code>show</code>:
# 1. Geben Sie den zweiten Buchstaben der Zeichenkette aus
# 2. Geben Sie das Wort **eating** aus
# 3. Geben Sie alles nach **Software** aus
# 4. Geben Sie alles vor **the** aus
# 5. Geben Sie die letzten 3 Buchstaben der Zeichenkette aus

# In[74]:


show = "Software is eating the world!"
print(show[1])
print(show[12:19])
print(show[8:])
print(show[:19])
print(show[-3:])


# ## Task 2.1.3 - if, elif, else Statements
# 
# Es gilt:
# - Wenn <code>x</code> 'true' ist, hat die Ausgabe '**x was True!**' zu erfolgen
# - sonst **'x was False!'**

# In[21]:


x = False
if x:
    print("x was True!")
else:
    print("x was False!")


# ## Task 2.2.4 - if, elif, else Statements
# 
# Es gilt:
# - Wenn <code>person</code> 'Georg' ist, hat die Ausgabe '**Welcome Georg**' zu erfolgen.
# - Ist <code>person</code> 'Jimmy', dann '**Welcome Jimmy**'.
# - Sonst '**Welcome, what is your name?**'

# In[22]:


person = 'Georg'
if person == "Georg":
    print("Welcome Georg")
elif person == "Jimmy":
    print("Welcome Jimmy")
else:
    print("Welcome, what is your name?")


# ## Task 2.2.5 - Loops, Lists, Dicts etc. 

# Iterieren Sie über die Liste <code>list1</code> und fügen Sie hierbei alle geraden Einträge der neu zu erstellenden Liste <code>even</code> und alle ungeraden der Liste <code>odd</code> hinzu. Setzen Sie den Modulo-Operator ein.

# In[75]:


list1 = [1,2,3,4,5,6,7,8,9,10]
even = []
odd = []

for i in list1:
    if i % 2 == 0:
        even.append(i)
    else:
        odd.append(i)
        
print("even:")
print(even)
print("odd:")
print(odd)


# ## Task 2.2.6 - Loops, Lists, Dicts etc. 
# Geben Sie die *Keys* als auch die *Values* des Dictionaries <code>d1</code> aus.

# In[32]:


d1 = {'k1':1,'k2':2,'k3':3}
for key, val in d1.items():
    print(key + ": " + str(val))


# ## Task 2.2.7 - Loops, Lists, Dicts etc. 
# Erstellen ein Dictionary mit ein paar Namenseinträgen. Erstellen Sie in Folge ein zweites Dictionary mit weiteren Namenseinträgen. Fügen Sie beide Dictionaries zusammen und stellen Sie hierbei sicher, dass im "neuen" Dictionary keine Duplikate vorhanden sind. (Anm.: Testszenario mit mind. einer Namensgleichheit. Die Überprüfung hat manuell zu erfolgen).

# In[77]:


n1, n2 = {1: "Peter", 2: "Albert", 3: "Max"}, {1: "Rudolf", 2: "Max", 3: "Anna"}

for key, val in n2.items():
    ok = True
    for i in n1:
        if val == n1[i]:
            ok = False
            break
    if ok:
        n1[len(n1) + 1] = val
        
print(n1)


# ## Task 2.2.8
# Erstellen Sie mithilfe der *Built-in Function* `range` eine Liste mit Werten, die folgende Werte enthält: `[0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]`. Details bzgl. Verwendung von `range` liefert diese [Quelle](https://docs.python.org/3.8/library/functions.html).

# In[84]:


l1 = range(0, 111, 10)
for i in l1:
    print(i)
#mit unpacking    
l1 = [*range(0, 111, 10)]
print(l1)

