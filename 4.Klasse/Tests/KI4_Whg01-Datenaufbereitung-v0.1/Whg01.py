#!/usr/bin/env python
# coding: utf-8

# In[84]:


import pandas as pd
import matplotlib.pyplot as plt


# # 1)
# ## a)

# In[15]:


df = pd.read_csv('train.csv', index_col=0)


# In[16]:


df.head(3)


# ## b)
# Standartseperator ist `,`

# # 2)

# In[17]:


print(f'Die Trainingsdaten des Datasets umfassen {df.shape[0]} Zeilen und {df.shape[1]} Spalten')


# # 3)

# In[12]:


df.info()


# ## a)
# Die Daten sind nicht ganz vollständig
# ## b)
# Die Spalte `Age` könnte mit dem Durchschnittsalter ergänzt werden

# # 4)
# ## a), b)

# In[57]:


titleList = []


def title(row):
    title = row['Name'].split(',')[1].split('.')[0].strip()
    if title not in titleList:
        titleList.append(title)
    return title


df['Title'] = df.apply(lambda row: title(row), axis=1)


# In[58]:


titleList


# In[74]:


df['Title'].head()


# ## c)

# In[61]:


title_dict = {
 "Capt": "Officer",
 "Col": "Officer",
 "Major": "Officer",
 "Jonkheer": "Royalty",
 "Don": "Royalty",
 "Sir": "Royalty",
 "Dr": "Officer",
 "Rev": "Officer",
 "the Countess": "Royalty",
 "Dona": "Royalty",
 "Mme": "Mrs",
 "Mlle": "Miss",
 "Ms": "Mrs",
 "Mr": "Mr",
 "Mrs": "Mrs",
 "Miss": "Miss",
 "Master": "Master",
 "Lady": "Royalty"
}


# ## d)

# In[69]:


df['Title'] = df['Title'].map(title_dict)


# ## e)

# In[ ]:


del df['Name']


# In[98]:


df.head()


# # 5)

# In[111]:


df.boxplot(column='Age')
plt.show()

